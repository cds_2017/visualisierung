#!/usr/bin/env bash

config_file='vis.config'

if [ -z "$1" ]
  then
    echo "usage ./pull_data.sh <password>"
    exit 1
fi

if [ ! -f $config_file ]; 
then
  echo "Configuration file not found!"
  exit 2
fi

. $config_file

password=$1
user='dsda'
wget --user ${user} --password ${password} http://fusion.cs.uni-jena.de/lehre/vis2018/data.zip -P ${data_folder}
wget --user ${user} --password ${password} http://fusion.cs.uni-jena.de/lehre/vis2018/meta.zip -P ${meta_folder}
wget -N --user ${user} --password ${password} http://fusion.cs.uni-jena.de/lehre/vis2018/sensoren.tsv -P ${sensor_folder}  
unzip ${data_folder}/data.zip -d ${data_folder}
unzip ${meta_folder}/meta.zip -d ${meta_folder}
rm -fr ${data_folder}/data.zip
rm -fr ${meta_folder}/meta.zip
