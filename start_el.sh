#!/usr/bin/env bash

config_file='vis.config'
if [ ! -f $config_file ]; 
then
  echo "Configuration file not found!"
  exit 2
fi

. $config_file

export ES_PATH_CONF=$elasticsearch_config
${elasticsearch_path}/bin/elasticsearch $@
