# ========================================
# Visualisierung, Data Cleansing
# Jonas Gütter, Paul Rudolph, Markus Unkel
# ========================================

# =========
# LIBRARIES
# =========

from __future__ import division

import numpy as np
import pandas as pd
import json

from os import path,makedirs
from shapely.wkt import loads 

import re

class DataCleanser:
    
    def __init__(self, data_dir, meta_dir, column_desc):
        self.data_dir = data_dir
        self.meta_dir = meta_dir
        self.column_desc = column_desc
        self.update_column_type()
        self.tsv_file = None
        self.ds_name = ''
        self.json_file = None
        self.ch_df = pd.DataFrame()
    
    def update_column_type(self):
        changes = {
            "startoffirstacqu0": "Date",
            "endoflastacquisi0": "Date",
            "antennalookdirec3": 'Character',
            "antennalookdirec0": 'Character',
            "antennalookdirec2": 'Character',
            "polarisationmode3": 'Character',
            "polarisationmode4": 'Character',
            "polarisationmode0": 'Character',
            "demidentifier0": 'Identifier',
            "identifier1": 'Identifier',
            "media_id0": 'Identifier',
            "unique_id": 'Identifier',
            "taptakeid0": 'Identifier',
            #"subscene_id0": 'Identifier',
            "productid": 'Identifier',
            "meandifferencebe0": 'Double'
        }
        for key, val in changes.items():
            for i in range(len(self.column_desc)):
                if 'id' in self.column_desc[i] and key == self.column_desc[i]['id']:
                    self.column_desc[i]['type'] = val

    def load_data(self, dataset_name):
        self.ch_df = pd.DataFrame()
        self.ds_name = dataset_name
        data_path = '{0}/{1}.tsv'.format(self.data_dir, dataset_name)
        meta_path = '{0}/{1}.json'.format(self.meta_dir, dataset_name)
        if not path.exists(meta_path) and not path.exists(data_pathz):
            pass
        else:
            self.tsv_file = pd.read_csv(data_path, sep='\t',header=None)
            json_data = json.load(open(meta_path))
            self.tsv_file.columns = json_data['columns']
            # load constants into dicts too
            for const in json_data['constants']:
                self.tsv_file[const['id']] = const['value']
                json_data['columns'].append(const['id'])
            self.json_file = json_data
            
    def combine_lat_long(self):
        # Combine the following columns to one comlumn, the order matters
        list_scene = ['latitude14',#ll
                      'latitude13',#lc
                      'latitude15',#lr
                      #'latitude17',#cc
                      'latitude20',#rr
                      'latitude18',#rc
                      'latitude19',#rl
                      'longitude14',#ll
                      'longitude13',#lc
                      'longitude15',#lr
                      # 'longitude17',#cc
                      'longitude20',#rr
                      'longitude18',#rc
                      'longitude19',#rl
                     ]
        # do the combining
        num_points = int(len(list_scene)/2)
        if set(list_scene).issubset(self.tsv_file.columns.values):
            self.json_file['columns'].append('geoscene')
            self.json_file['columns'].append('geoscenecenter')
            self.tsv_file['geoscene'] = 'POLYGON(('
            self.tsv_file['geoscenecenter'] = 'POINT(' +  self.tsv_file['latitude17'].map(str) + ' '+ self.tsv_file['longitude17'].map(str) + ')'
            for i in range(num_points):
                lat_name = list_scene[i]
                long_name = list_scene[i + num_points]
                self.tsv_file['geoscene'] +=  self.tsv_file[lat_name].map(str) + " " + self.tsv_file[long_name].map(str) 
                if i == num_points - 1:
                    self.tsv_file['geoscene']  += '))'
                else:
                    self.tsv_file['geoscene']  += ', '
            # add entry in the description dict
            desc_gs_entry = {'id': 'geoscene', 'type': 'GeoObject', 'hierarchy':'[sceneParameter]'}
            desc_gsc_entry = {'id': 'geoscenecenter', 'type': 'GeoObject', 'hierarchy':'[sceneParameter]'}
            already_set = False
            for i in range(len(self.column_desc)):
                if 'id' in self.column_desc[i]:
                    if self.column_desc[i]['id'] == 'geoscene':
                        already_set = True
            if not already_set:
                self.column_desc.append(desc_gs_entry)
                self.column_desc.append(desc_gsc_entry)
    # =========
    # CLEANSING
    # =========
    def do_cleansing(self):
        self.combine_lat_long()
        self.change_char_objs()
        self.change_date_objs()
        self.change_text_objs()
        self.change_identifier_objs()
        self.change_geo_objs()
        self.change_boolean_objs()
        
    def change_char_objs(self):
        col_list = self.get_type_list('Character')
        for col in col_list: 
            if col in self.tsv_file.columns.values:
                #delete unecessary whitespaces
                changes = self.tsv_file[col].apply(lambda x: str(x).strip() if str(x) != 'nan' else np.nan)
                changes = changes.apply(lambda x: x if len(str(x)) > 0 else np.nan)
                ch_list = changes.map(str) != self.tsv_file[col].map(str)
                ch_dict = dict()
                ch_dict['unique_id'] = self.tsv_file['unique_id'][ch_list]
                ch_dict['new_val'] = changes[ch_list]
                ch_dict['old_val'] = self.tsv_file[col][ch_list]
                ch_dict['name'] = self.ds_name
                ch_dict['column'] = col
                self.ch_df = self.ch_df.append(pd.DataFrame(ch_dict))
                self.tsv_file[col] = changes
                
    def change_boolean_objs(self):
        col_list = self.get_type_list('Boolean')
        for col in col_list: 
            if col in self.tsv_file.columns.values:
                #delete unecessary whitespaces
                changes = self.tsv_file[col].apply(lambda x: str(x).strip() if str(x) != 'nan' else np.nan)
                changes = changes.apply(lambda x: x if len(str(x)) > 0 else np.nan)
                changes = changes.apply(lambda x: 'true' if x == '1' else x)
                changes = changes.apply(lambda x: 'true' if x == '1.0' else x)
                changes = changes.apply(lambda x: 'false' if x == '0.0' else x)
                changes = changes.apply(lambda x: 'false' if x == '0' else x)
                ch_list = changes.map(str) != self.tsv_file[col].map(str)
                ch_dict = dict()
                ch_dict['unique_id'] = self.tsv_file['unique_id'][ch_list]
                ch_dict['new_val'] = changes[ch_list]
                ch_dict['old_val'] = self.tsv_file[col][ch_list]
                ch_dict['name'] = self.ds_name
                ch_dict['column'] = col
                self.ch_df = self.ch_df.append(pd.DataFrame(ch_dict))
                self.tsv_file[col] = changes
                           
    def change_date_objs(self):
        pattern_date = re.compile("^[0-9]{4}-[01][0-9]-[0-3][0-9] [012][0-9]:[0-5][0-9]:[0-5][0-9][.][0-9]{3}$")
        col_list = self.get_type_list('Date')
        for col in col_list:
            if col in self.tsv_file.columns.values:
                for obj in self.tsv_file[col].unique():
                    if not pattern_date.match(str(obj)):
                        if str(obj) != 'nan':
                            # change every date to the correct date formate
                            changes = self.tsv_file[col].apply(lambda x: pd.to_datetime(x).strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] if str(x) != 'nan' else np.nan)
                            ch_list = changes.map(str) != self.tsv_file[col].map(str)
                            ch_dict = dict()
                            ch_dict['unique_id'] = self.tsv_file['unique_id'][ch_list]
                            ch_dict['new_val'] = changes[ch_list]
                            ch_dict['old_val'] = self.tsv_file[col][ch_list]
                            ch_dict['name'] = self.ds_name
                            ch_dict['column'] = col
                            self.ch_df = self.ch_df.append(pd.DataFrame(ch_dict))
                            self.tsv_file[col] = changes                    
                            break

    def change_text_objs(self):
        #change text objects
        col_list = self.get_type_list('Text')
        # change inconsistency in these columns
        ad_col = ['orbitdirection1', 'orbitdirection0', 'orbitdirection4']
        domain_replace = {'ascending':'A','descending': 'D','ASCENDING':'A'}
        # dict for typo correction
        region1_dict = {
            'Appennino': 'Apennine',
            'Apennines': 'Apennine',
            'Atlantik': 'Atlantic',
            'Geenland':'Greenland',
            'Baltic': 'Baltic States',
            'Baltics': 'Baltic States',
            'Black': 'Black Sea',
            'Bulgar': 'Bulgaria',
            'Bulgariaia': 'Bulgaria',
            'Cypros': 'Cyprus',
            'Great': 'UK',
            'Italia': 'Italy',
            'Krete': 'Crete',
            'Morocco': 'Marocco',
            'North': 'North Sea',
            'Slavakia': 'Slovakia',
            'United': 'UK',
            'Luxemburg': 'Luxembourg',
            'Belorus': 'Belarus',
            'Carpathian':'Carpathian Mountains',
            'Carpates':'Carpathian Mountains',
            'Carpathian Mountains Mountains': 'Carpathian Mountains',
            'Golfe': 'Gulf',
            'Gole': 'Gulf',
            'Golf': 'Gulf',
            'U K': 'UK',
            'KIreland': 'Ireland',
            'Republic': 'Rep',
            'Repub': 'Rep',
            'sea': 'Sea',
            'rep': 'Rep',
            'Aegea': 'Aegean',
            'Lituania':'Lithuania',
            'Mediterranean': 'Meditterranean',
            'Mediteranean': 'Meditterranean',
            'Norvegian':'Norwegian',
            'Sicilia': 'Sicily',
            'Statess':'States',
            'Turquey': 'Turkey',
            'Tyrrenian': 'Tyrrhenian',
            'Tyrrhanean': 'Tyrrhenian',
            'Tyrrhenian': 'Tyrrhenian',
            'U,Ireland' : 'Ireland',
            'Baltic States States': 'Baltic States',
            'Black Sea Sea': 'Black Sea',
            'Atlantic Ocean': 'Atlantic',
            'Atlantic Sea': 'Atlantic',
            'Baltic States Sea': 'Baltic Sea',
            ' du ': ' of ',
            ' de ': ' of ',
            'North Sea Sea': 'North Sea',
            'Byelarus': 'Belarus',
            'UK UK Britain':'UK',
            'UK Britain':'UK',
            'UK Kingdom': 'UK',
            ',,':',',
            ' ,':',',
            'Aegeann': 'Aegean',
            'Aegan': 'Aegean',
            'Meditterranean': 'Mediterranean Sea',
            'Meditterranean Sea Sea': 'Mediterranean Sea',
            'U,K': 'UK',
            'UK,UK': 'UK',
            'Corse':'Corsica',
            'Czech': 'Czech Rep',
            'Czech Rep Rep': 'Czech Rep',
            'of Riga':'Gulf of Riga',
            'Gulf Gulf of Riga':'Gulf of Riga',
            'North Sea Ireland':'North Ireland',
            'Mediterranean Sea Sea': 'Mediterranean Sea'
        }
        region0_dict = {
            'EUROPE':'Europe',
            'MEDITERRANEAN':'Mediterranean',
            'ATLANTIC':'Atlantic',
            'MADEIRA': 'Madeira',
        }
        def reg1_typos(obj):
            if str(obj) == 'nan':
                return np.nan
            pattern_rep = re.compile(r'(_|\.)')
            matches = re.sub(pattern_rep,' ',str(obj))
            pattern_rep = re.compile(r'(\\)')
            matches = re.sub(pattern_rep,',', matches)
            matches = re.sub(r"(\w)([A-Z])", r"\1,\2", matches)
            for key,val in region1_dict.items():
                matches = matches.replace(key, val)
            matches = matches.split()
            matches = " ".join(matches)
            matches = ",".join(sorted(matches.split(',')))
            return matches

        def reg0_typos(obj):
            if str(obj) == 'nan':
                return np.nan
            matches = str(obj)
            for key,val in region0_dict.items():
                matches = matches.replace(key, val)
            matches = matches.split()
            matches = " ".join(matches)
            return matches
        # change data
        for col in col_list:
            if col in self.tsv_file.columns.values:
                changes = self.tsv_file[col]
                if col in ad_col:
                    changes = changes.replace(domain_replace)
                #delete unecessary whitespaces
                changes = changes.apply(lambda x:  str(x).strip() if str(x) != 'nan' else np.nan)
                if col == 'region1':
                    #correct typos region1
                    changes = changes.apply(reg1_typos)
                elif col == 'region0':
                    #correct typos rehion0
                    changes = changes.apply(reg0_typos)
                changes = changes.apply(lambda x: x if len(str(x)) > 0 else np.nan)
                ch_list = changes.map(str) != self.tsv_file[col].map(str)
                ch_dict = dict()
                ch_dict['unique_id'] = self.tsv_file['unique_id'][ch_list]
                ch_dict['new_val'] = changes[ch_list]
                ch_dict['old_val'] = self.tsv_file[col][ch_list]
                ch_dict['name'] = self.ds_name
                ch_dict['column'] = col
                self.ch_df = self.ch_df.append(pd.DataFrame(ch_dict))
                self.tsv_file[col] = changes

# change identifier objects
    def change_identifier_objs(self):
        col_list = self.get_type_list('Identifier')
        for col in col_list:
            if col in self.tsv_file.columns.values:
                #delete unecessary whitespaces
                changes = self.tsv_file[col].apply(lambda x:  str(x).strip() if str(x) != 'nan' else np.nan)
                changes = changes.apply(lambda x: x if len(str(x)) > 0 else np.nan)
                ch_list = changes.map(str) != self.tsv_file[col].map(str)
                ch_dict = dict()
                ch_dict['unique_id'] = self.tsv_file['unique_id'][ch_list]
                ch_dict['new_val'] = changes[ch_list]
                ch_dict['old_val'] = self.tsv_file[col][ch_list]
                ch_dict['name'] = self.ds_name
                ch_dict['column'] = col
                self.ch_df = self.ch_df.append(pd.DataFrame(ch_dict))
                self.tsv_file[col] = changes
    
    def change_geo_objs(self):
        col_list = self.get_type_list('GeoObject')
        pattern_geo = re.compile("(POLYGON|LINESTRING|POINT)([( ]+)([+ \-\.]?(\d+)?([ \.]\d+[ ,)]+)?)+")
        pattern_nonnum = re.compile(r'(POLYGON|LINESTRING|POINT|\(|\)|,)')
        l_func = lambda x: str(x if x % 1 else int(x))
        def change_data(obj):
            if not pattern_geo.match(str(obj)):
                return np.nan
            matches = re.sub(pattern_nonnum,'',str(obj)).strip().split()
            matches = [float(x) for x in matches]
            if set(matches).issubset(set([-180.0,-90.0])):
                return np.nan
            # north pole check
            if set(matches).issubset(set([0.0])):
                return np.nan
            # range correction
            k = 0
            for num in matches:
                if k%2 == 0 and (num > 180 or num < -180):
                    return np.nan
                elif k%2 == 1 and (num > 90 or num < -90):
                    return np.nan
                k = k + 1
            # special care for polygons
            if not re.compile("(POLYGON).*").match(str(obj)):
                return obj
            vertices = list()
            if matches:
                lst = [float(m) for m in matches]
                vertices = list(zip(lst[0::2], lst[1::2]))
            if len(vertices) > 1 and (vertices[0] != vertices[-1]):
                vertices.append(vertices[0])
            # change to point if it only has 2 points
            if len(set(vertices)) == 1:
                ret_str =  "{0} {1}".format(l_func(vertices[0][0]), l_func(vertices[0][1]))
                return "POINT(" + ret_str + ")"
            # change to linestring if it only has 2 (unique) points
            elif len(set(vertices)) == 2:   
                vertices = set(vertices)
                ret_str =  ', '.join([str(i[0])+ ' ' +str(i[1]) for i in vertices])
                return "LINESTRING(" + ret_str + ")"
            ret_str =  ', '.join([str(i[0])+ ' ' +str(i[1]) for i in vertices])
            ret_str = "POLYGON((" + ret_str + "))"
            ret_str = loads(ret_str).buffer(0)
            ret_str = ret_str.wkt
            # remove invalid polygons (converting failed)
            # might be interesting to keep multpolygons though
            if 'EMPTY' in ret_str or 'MULTIPOLYGON' in ret_str:
                return np.nan
            else:
                # make polygons counter clockwise
                matches = re.sub(pattern_nonnum,'',str(ret_str)).strip().split()
                vertices = list()
                if matches:
                    lst = [float(m) for m in matches]
                    vertices = list(zip(lst[0::2], lst[1::2]))
                res = 0.0
                for i in range(len(vertices)):
                    v1 = vertices[i]
                    v2 = vertices[(i + 1) % len(vertices)]
                    res = res + (v2[0] - v1[0]) * (v2[1] + v1[1]);
                if res > 0.0:
                    vertices = vertices[::-1]
                ret_str =  ', '.join(["{0} {1}".format(l_func(i[0]), l_func(i[1])) for i in vertices])
                # some polygons have still problems to be displayed the right way, kepp them as LINESTRING (maybe todo here)
                if len(vertices) > 1 and (vertices[0] != vertices[-1]):
                    return np.nan
                else:
                    return "POLYGON((" + ret_str + "))"
        for col in col_list:
            if col in self.tsv_file.columns.values:
                changes = self.tsv_file[col].apply(change_data)
                ch_list = changes.map(str) != self.tsv_file[col].map(str)
                ch_dict = dict()
                ch_dict['unique_id'] = self.tsv_file['unique_id'][ch_list]
                ch_dict['new_val'] = changes[ch_list]
                ch_dict['old_val'] = self.tsv_file[col][ch_list]
                ch_dict['name'] = self.ds_name
                ch_dict['column'] = col
                self.ch_df = self.ch_df.append(pd.DataFrame(ch_dict))
                self.tsv_file[col] = changes
                
    def get_type_list(self, sel_type):
        ret_list = list()
        for col in self.column_desc:
            if 'id' in col:
                if sel_type == col['type']:
                    ret_list.append(col['id'])
        return ret_list
    
    def save_changes(self, save_path):
        if len(self.ch_df) != 0:
            save_path = save_path + '/changes'
            if not path.exists(save_path):
                makedirs(save_path)
            self.ch_df.to_csv(save_path+'/'+self.ds_name, sep='\t',index=None)
