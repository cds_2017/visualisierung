# ========================================
# Visualisierung, Setup Elasticsearch
# Jonas Gütter, Paul Rudolph, Markus Unkel
# ========================================

# =========
# LIBRARIES
# =========
from __future__ import division

import numpy as np
import pandas as pd
import json

from os import listdir
from fnmatch import filter as fnfilter
from shapely.wkt import loads 

from itertools import product
from collections import Counter
import re
import operator
from leven import levenshtein

# =====
# CHECK
# =====
class DataChecker:
    def __init__(self, tsv_data, column_desc):
        self.tsv_data = tsv_data
        self.column_desc = column_desc
    
    def check_geo(self):
        print("----------------------------")
        print("Checking columns with geo data...")
        pattern_geo = re.compile("(POLYGON|LINESTRING|POINT)([( ]+)([+ \-\.]?(\d+)?([ \.]\d+[ ,)]+)?)+")
        pattern_nonnum = re.compile(r'(POLYGON|LINESTRING|POINT|\(|\)|,)')

        col_list = self.get_type_list('GeoObject')

        def check_ccw(polygon_string):
            matches = re.sub(pattern_nonnum,'',polygon_string).strip().split()
            vertices = list()
            if matches:
                lst = [float(m) for m in matches]
                vertices = list(zip(lst[0::2], lst[1::2]))
            res = 0.0
            for i in range(len(vertices)):
                v1 = vertices[i]
                v2 = vertices[(i + 1) % len(vertices)]
                res = res + (v2[0] - v1[0]) * (v2[1] + v1[1]);
            return res <= 0.0

        list_nontype =  list()
        dict_col = dict()
        count_cc = 0
        count_rv = 0
        count_sp = 0
        count_iv = 0
        count_nr = 0
        #run checks
        for col in col_list:
            if col in self.tsv_data.columns.values:
                per_nan = self.tsv_data[col].isnull().sum()/len(self.tsv_data[col])
                if per_nan > 0:
                    dict_col[col] = per_nan
                for obj in self.tsv_data[col].unique():
                    if not pattern_geo.match(str(obj)):
                        list_nontype.append(obj)
                    else:
                        #range checks
                        k = 0
                        matches = re.sub(pattern_nonnum, '', obj).strip().split()
                        for number in matches:
                            num = float(number)
                            if k%2 == 0 and (num > 180 or num < - 180):
                                count_rv = count_rv + 1
                                break
                            elif k%2 == 1 and (num > 90 or num < - 90):
                                count_rv = count_rv + 1
                                break
                            k = k + 1
                        # south pole check
                        if str(obj) == 'POLYGON((-180 -90, -180 -90))' or str(obj) ==  'POLYGON((-180.0 -90.0, -180.0 -90.0))':
                            count_sp = count_sp + 1
                        elif re.compile("(POLYGON).*").match(obj):
                            # clockwise check
                            if not check_ccw(obj):
                                count_cc = count_cc + 1
                            if matches[0] != matches[-2] and matches[1] != matches[-1]:
                                count_nr += 1
                            # invalid check
                            elif not loads(str(obj)).is_valid:
                                count_iv += 1                                
                                                                        
        # print results
        print("Non geo entries: %s" % str(len(list_nontype)))
        print("Number of clockwise entries: %s" % str(count_cc))
        print("Number of columns with south pole polygons: %s" % str(count_sp))
        print("Number of range violations: %s" % str(count_rv))
        print("Number of polygon with non_closed_ring: %s" % str(count_nr))
        print("Number of invalid polygons: %s" % str(count_iv))
        print("Percentage of missing values: %s" % str(dict_col))
    
    def check_date(self):
        print("----------------------------")
        print("Checking columns with date data...")
        pattern_date = re.compile("^[0-9]{4}-[01][0-9]-[0-3][0-9] [012][0-9]:[0-5][0-9]:[0-5][0-9][.][0-9]{3}$")
        col_list = self.get_type_list('Date')

        list_nontype =  list()
        dict_col = dict()

        for col in col_list:
            if col in self.tsv_data.columns.values:
                per_nan = self.tsv_data[col].isnull().sum()/len(self.tsv_data[col])
                if per_nan > 0:
                    dict_col[col] = per_nan
                    # check formate violations
                    for obj in self.tsv_data[col].unique():
                        if not pattern_date.match(str(obj)):
                            if str(obj) != 'nan':
                                list_nontype.append(obj)
        #print results
        print("Count of entries which don't fit formate: %s" % str(len(list_nontype)))
        print("Count of columns with nans: %s" % str(len(dict_col)))
        print("Percentage of missing values: %s" % str(dict_col))

    def check_text(self):
        # check text data
        # check for inconsitencies
        # check for mightbe typos (with levenshtein)
        print("----------------------------")
        print("Checking columns with text data...")
        pattern_time = re.compile("^[0-9]{4}-[01][0-9]-[0-3][0-9]")
        col_list = self.get_type_list('Text')
        get_ld = ['region0', 'region1']
        list_datetype =  list()
        list_numtype =  list()
        list_dv = list()
        domain_violation = [
        (['orbitdirection1', 'orbitdirection0', 'orbitdirection4'],['A','D','mixed']),
        ]
        dict_ld = dict()
        dict_col = dict()

        count_ld = 0
        count_ws = 0
        for col in col_list:
            if col in get_ld:
                dict_ld[col] = list()
                if col in self.tsv_data.columns.values:
                    if col in get_ld:
                         dict_ld[col] += list(self.tsv_data[col][~(self.tsv_data[col].isnull())].unique())
                    for item in domain_violation:
                        if col in item[0]:
                            list_dv += list(set(self.tsv_data[col][~(self.tsv_data[col].isnull())].unique()) - set(item[1]))
                    per_nan = self.tsv_data[col].isnull().sum()/len(self.tsv_data[col])
                    if per_nan > 0:
                        dict_col[col] = per_nan
                    # check if only numeric
                    try:
                        pd.to_numeric(self.tsv_data[col].unique())
                        list_numtype.append(col)
                        break
                    except:
                        pass
                    # check if only dates
                    for obj in self.tsv_data[col].unique():
                        if pattern_time.match(str(obj)):
                            list_datetype.append(col)
                            break
                    # check for unessary whitepaces (at the end and beginning)
                    for obj in self.tsv_data[col].unique():
                        if str(obj) != str(obj).strip():
                            count_ws = count_ws + 1
        # levenshtein check for typos
        for value in dict_ld.values():
            df = [x.upper() for x in value if str(x) != 'nan']
            dist = [levenshtein(*x) for x in product(df, repeat=2)]
            dist_df = pd.DataFrame(np.array(dist).reshape(len(df), len(df)))
            for i in range(0, dist_df.shape[0]):
                for k in range(0, dist_df.shape[1]):
                    if i >= k:
                        pass
                    elif dist_df[i][k] < 3:
                        count_ld += 1
        # print results
        print("Count Levenshtein matches: %s" % count_ld)
        print("Count domain violations: %s" % len(list_dv))                    
        print("Strings with unecessary whitspaces: %s" % str(count_ws))                    
        print("Columns with only numeric entries: %s" % len(list_numtype))
        print("Columns with only date entries: %s" % len(list_datetype))
        print("Percentage of missing values: %s" % str(dict_col))

    def check_boolean(self):
        # print all different kind of values in boolean over the whole dataset
        print("----------------------------")
        print("Checking columns with boolean data...")

        col_list = self.get_type_list('Boolean')

        list_nontype =  list()
        dict_col = dict()

        for col in col_list:
            if col in self.tsv_data.columns.values:
                per_nan = self.tsv_data[col].isnull().sum()/len(self.tsv_data[col])
                if per_nan > 0:
  
                    dict_col[col] = per_nan
                # check values
                for obj in self.tsv_data[col].unique():
                    if obj != 0:
                        list_nontype.append(obj)
        # print results
        print("Boolean type error: %s" % str(list_nontype))
        print("Percentage of missing values: %s" % str(dict_col))

    def check_identifier(self):
        # check Identifier data
        # check if all identifier are unique
        # check existence of identifiers
        print("----------------------------")
        print("Checking columns with Identifier data...")
        col_list = self.get_type_list('Identfier')

        diff = lambda l1,l2: [x for x in l1 if x not in l2]

        list_nan = list()
        count_ws = 0

        for col in col_list:
            if col in self.tsv_data.columns.values:
                uni_entries = list(self.tsv_data[col].unique())
                entries = list(self.tsv_data[col])
                if pd.isnull(uni_entries).sum() != 0:
                    list_nan.append(col)
                for obj in self.tsv_data[col].unique():
                    if str(obj).strip() != str(obj):
                        count_ws += 1
        # print results
        print("Count nan in Identifiers: %s" % len(list_nan))
        print("Count of unecessary white spaces: %s" % count_ws)
    
    def check_character(self):
        #check characters
        # check existence of identifiers
        print("----------------------------")
        print("Checking columns with Character data...")
        col_list = self.get_type_list('Character')

        diff = lambda l1,l2: [x for x in l1 if x not in l2]

        list_nontype = list()
        count_ws = 0

        for col in col_list:
            if col in self.tsv_data.columns.values:
                for obj in self.tsv_data[col].unique():
                    if len(str(obj)) != 1:
                        list_nontype.append(obj)
                    if str(obj).strip() != str(obj):
                        count_ws += 1
        print("Type errors: %s" % str(list_nontype))
        print("Count of unecessary white spaces: %s" % count_ws)
        
    def check_double(self):
        # check doubles
        # check for might be range violations and outliers
        print("----------------------------")
        print("Checking columns with Double data...")

        col_list = self.get_type_list('Double')

        dict_outliers = dict()
        dict_vals = dict()
        list_rv = list()
        list_nontype = list()
        list_const = list()
        
        range_violation = [
        (['averageheighterr2', 'averageheighterr3', 'perc90heighterro0', 'perc90heighterro1', 'percrelheighterr0', 'percrelheighterr1'
        ],np.finfo(float).max,0),
        (['percentageofheig0', 'percentageoflayo0', 'percentageofvoid0', 'percentageofvoid1'],100,0),
        (['longitude19', 'longitude18', 'longitude20', 'longitude17', 'longitude14', 'longitude13', 'longitude15'],180, -180),
        (['frame_ll_lat0', 'frame_lr_lat0', 'frame_ul_lat0', 'frame_ur_lat0', 'latitude17', 'latitude18', 'latitude14', 'latitude19', 'latitude13', 'latitude15', 'latitude20', 'scene_center_lat0'],
        90,-90),
        ]

        for col in col_list:
            dd = list()
            if col in self.tsv_data.columns.values:
                try:
                    x = self.tsv_data[col]
                    dd = dd + list(x[~pd.isnull(x)])
                except:
                    pass
                try:
                    dd = np.array(dd)
                    # check for range violation
                    for rv in range_violation:
                        if col in rv[0]:
                            rv_min = rv[2]
                            rv_max = rv[1]
                            if dd.min() < rv_min or dd.max() > rv_max:
                                list_rv.append(col)
                    # outlier check
                    d = dd[~(np.abs(dd-np.mean(dd))<(4*np.std(dd)))]
                    if np.std(dd) == 0:
                        list_const.append((col, np.mean(dd)))
                    if len(d) > 0:
                        dict_outliers[col ] = len(d)
                        dict_vals[col] = [dd.mean(), dd.std(),dd.max(),dd.min(), d]
                except:
                    list_nontype.append(col)
        # print results
        print("%s Range violations in \n%s" % (len(list_rv), "\n".join(list_rv)))
        print("Outliers detected in %s columns of %s" % (len(dict_vals), len(col_list)))
        print("Non numeric columns count %s" % (len(list_nontype)))
    
    def check_integer(self):
        # check integers
        # check outliers
        #check range violations
        print("----------------------------")
        print("Checking columns with Integer data...")

        col_list = self.get_type_list('Integer')

        dict_outliers = dict()
        dict_vals = dict()
        list_rv = list()
        list_nontype = list()

        range_violation = [
        ]

        list_const = list()

        for col in col_list:
            dd = list()
            if col in self.tsv_data.columns.values:
                try:
                    x = self.tsv_data[col]
                    dd = dd + list(x[~pd.isnull(x)])
                except:
                    pass
                try:
                    dd = np.array(dd)
                    # check range violations
                    for rv in range_violation:
                        if col in rv[0]:
                            rv_min = rv[2]
                            rv_max = rv[1]
                            if dd.min() < rv_min or dd.max() > rv_max:
                                list_rv.append(col)
                    # check outliers
                    d = dd[~(np.abs(dd-np.mean(dd))<(4*np.std(dd)))]
                    if np.std(dd) == 0:
                        list_const.append((col, np.mean(dd)))
                    if len(d) > 0:
                        dict_outliers[col ] = len(d)
                        dict_vals[col ] = [dd.mean(), dd.std(),dd.max(),dd.min(), d]
                except:
                    list_nontype.append(col)
        # print results
        print("%s Range violations in \n%s" % (len(list_rv), "\n".join(list_rv)))
        print("Outliers detected in %s columns of %s" % (len(dict_vals), len(col_list)))
        print("Non numeric columns count %s" % (len(list_nontype)))
    
    def do_checks(self):
        self.check_integer()
        self.check_double()
        self.check_character()
        self.check_text()
        self.check_geo()
        self.check_date()
        self.check_boolean()

    def get_type_list(self, sel_type):
        ret_list = list()
        for col in self.column_desc:
            if 'id' in col:
                if sel_type == col['type']:
                    ret_list.append(col['id'])
        return ret_list

