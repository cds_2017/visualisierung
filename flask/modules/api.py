import sys
sys.path.append('../../elasticsearch')
import wrapper_es
import json
import pandas as pd
import numpy as np


class API_QUERY():
    def __init__(self):
        self.filter_map = {
            "Text": self.filter_categorial,
            "Character": self.filter_categorial,
            "Identifier": self.filter_categorial,
            "Date": self.filter_date,
            "Integer": self.filter_numeric,
            "Double": self.filter_numeric
        }

    # exists query of dimensions
    def dimensions(self, dimensions):
        query = [{"exists": {"field": str(dim)}} for dim in dimensions]
        return query

    def filter_categorial(self, option):
        return {"terms": {option['id']: option['config']['keywords']}}

    def filter_date(self, option):
        return {
            "range": {
                option['id']: {
                    "gte": option['config']['gte'],
                    "lt": option['config']['lte'],
                    "format": "epoch_millis"
                }
            }
        }

    def filter_numeric(self, option):
        return {
            "range": {
                option['id']: {
                    "gte": option['config']['min'],
                    "lte": option['config']['max']
                }
            }
        }

    # filter all filter options of custom dashboard
    def filter(self, is_filter_data, dimension, options):
        query = list()
        for opt in options:
            id_name = opt['id']
            id_type = opt['type']
            if id_name != dimension or not is_filter_data:
                query.append(self.filter_map[id_type](opt))
        return query

    # get geographic data
    def filter_geo(self, dimension, type, relation, coordinates):
        query = {
            "geo_shape": {
                dimension: {
                    "shape": {
                        "type": type,
                        "coordinates": coordinates
                    },
                    "relation": relation
                }
            }
        }
        return query


class API():
    def __init__(self, host='localhost', port=9200):
        self.es = wrapper_es.WrapperES()
        self.config = {'max_result_window': 9999}
        self.query = API_QUERY()
        self.init_dimensions_table = None

    def dimension_properties(self, dimension_list):
        query = {
            "query": {
                "terms": {
                    "_id": dimension_list
                }
            },
            "size": self.config['max_result_window'],
            "_source": ["id", "label", "desc", "hierarchy", "type"]
        }
        return self.es.query(self.es.idx_desc, query)['hits']['hits']

    def dimension_count_query(self, dimension, selected_dimensions):
        query_cols = []
        if selected_dimensions:
            query_cols = [{
                "exists": {
                    "field": str(field_name)
                }
            } for field_name in selected_dimensions]
        if dimension:
            query_cols.append({"exists": {"field": str(dimension)}})
        query = {
            "query": {
                "bool": {
                    "must": query_cols
                },
            },
            "size": 0
        }
        return query

    def dimension_list_count(self, dimension_list, selected_dimensions):
        query_arr = []
        for key_val in dimension_list:
            query_arr.append({'index': self.es.idx_data})
            query_arr.append(
                self.dimension_count_query(key_val, selected_dimensions))

        request = ''
        for each in query_arr:
            request += '%s \n' % json.dumps(each)

        resp = self.es.mquery(request)

        column_counts = [r['hits']['total'] for r in resp['responses']]

        return column_counts

    def dimension_list_distinct_query(self, dimension, selected_dimensions):
        query_cols = []
        if selected_dimensions:
            query_cols = [{
                "exists": {
                    "field": str(field_name)
                }
            } for field_name in selected_dimensions]
        if dimension:
            query_cols.append({"exists": {"field": str(dimension)}})
        query = {
            "query": {
                "bool": {
                    "must": query_cols
                },
            },
            "size": 0,
            "aggs": {
                "distinct": {
                    "terms": {
                        "field": dimension,
                        "size": 9999,  # self.config['max_result_window']
                    }
                }
            }
        }
        return query

    def dimension_list_distinct_counts(self, dimension_list,
                                       selected_dimensions):
        query_arr = []
        for key_val in dimension_list:
            query_arr.append({'index': self.es.idx_data})
            query_arr.append(
                self.dimension_list_distinct_query(key_val,
                                                   selected_dimensions))

        request = ''
        for each in query_arr:
            request += '%s \n' % json.dumps(each)

        resp = self.es.mquery(request)
        distinct_counts = []
        for r in resp['responses']:
            try:
                counts = len(r['aggregations']['distinct']['buckets'])
                if counts == self.config["max_result_window"]:
                    distinct_counts.append(
                        ">=" + str(self.config["max_result_window"]))
                else:
                    distinct_counts.append(
                        len(r['aggregations']['distinct']['buckets']))
            except:
                distinct_counts.append("")

        return distinct_counts

    def plot_dimensions_table(self, selected_dimensions):
        query_dims = [{
            "exists": {
                "field": str(field)
            }
        } for field in selected_dimensions]
        query = {
            "query": {
                "bool": {
                    "must": query_dims
                }
            },
            "size": 0,
            "aggs": {
                "filenames": {
                    "terms": {
                        "field": "filename.keyword",
                        "size": self.config['max_result_window']
                    }
                }
            }
        }
        file_names_search = self.es.query(self.es.idx_data, query)
        file_names = [
            name['key'] for name in file_names_search['aggregations'][
                'filenames']['buckets']
        ]
        query_unique_ids = {
            "query": {
                "terms": {
                    "_id": file_names
                }
            },
            "size": 0,
            "aggs": {
                "ids": {
                    "terms": {
                        "field": "ids.keyword",
                        "size": self.config['max_result_window']
                    }
                }
            }
        }
        unique_ids_search = self.es.query(self.es.idx_file_names,
                                          query_unique_ids)
        dimensions_ids = [
            col['key']
            for col in unique_ids_search['aggregations']['ids']['buckets']
            if col['key'] not in selected_dimensions
        ]
        # get types of unique_dimensions
        dimensions_props = self.dimension_properties(dimensions_ids)
        # dimension names
        dimensions_names = []
        for i, prop_source in enumerate(dimensions_props):
            prop = prop_source['_source']
            name = ""
            if prop['hierarchy']:
                name += prop['hierarchy'][0] + " | "
            try:
                name += prop['label']
            except:
                print("ERROR_NO_LABEL_IN_DESCRIPTION: COLUMN_ID is",
                      dimensions_ids[i])
            dimensions_names.append(name)
        # overwrite dimensions ids if in description especially dimensions does not appear
        dimensions_ids = [prop['_source']['id'] for prop in dimensions_props]
        # dimensions types
        dimensions_types = [
            prop['_source']['type'] for prop in dimensions_props
        ]
        # dimensions description
        dimensions_desc = []
        for prop in dimensions_props:
            try:
                dimensions_desc.append(prop['_source']['desc'])
            except:
                print("ID", prop['_source']['id'], "HAS NO DESC")
                dimensions_desc.append("No Description")
        # dimension counts
        dimensions_count = self.dimension_list_count(dimensions_ids,
                                                     selected_dimensions)
        # dimension distinct value counts
        dimensions_distinct = self.dimension_list_distinct_counts(
            dimensions_ids, selected_dimensions)
        # dimension table
        table = pd.DataFrame(
            columns=["id", "name", "type", "count", "distinct", "desc"])
        table['id'] = dimensions_ids
        table['name'] = dimensions_names
        table['type'] = dimensions_types
        table['count'] = dimensions_count
        table['distinct'] = dimensions_distinct
        table['desc'] = dimensions_desc
        # save to API as initial dimensions table
        if self.init_dimensions_table is None:
            self.init_dimensions_table = table

        return table.to_json(orient='split')

    def uploaded_data(self):
        query = {
            "query": {
                "match_all": {}
            },
            "_source": ['file_name', 'objects', 'update_date'],
            "size": self.config['max_result_window']
        }
        uploaded_files = {
            x['_source']['file_name']: (x['_source']['objects'],
                                        x['_source']['update_date'])
            for x in self.es.query('dlr_file_names', query)['hits']['hits']
        }
        return uploaded_files

    # FILTER DATA: Categorial
    def _filter_categorial(self, dimension, selected_dimensions,
                           filter_options, request_options):
        # extract filter_dimensions
        filter_dimensions = [filter_opt['id'] for filter_opt in filter_options]
        # dimension, selected dimensions and filter dimensions must exist
        query_dimensions = self.query.dimensions(
            selected_dimensions + filter_dimensions)
        is_filter_data = True
        query_filter = self.query.filter(is_filter_data, dimension,
                                         filter_options)

        query = {
            "query": {
                "bool": {
                    "must": query_dimensions,
                    "filter": {
                        "bool": {
                            "must": query_filter
                        }
                    }
                },
            },
            "size": 0,
            "aggs": {
                "values": {
                    "terms": {
                        "field": dimension,
                        "size": 999
                    }
                }
            }
        }
        dimension_unique_values_count = self.es.query(
            self.es.idx_data, query)['aggregations']['values']['buckets']
        keys = [el["key"] for el in dimension_unique_values_count]
        doc_counts = [el["doc_count"] for el in dimension_unique_values_count]
        table = pd.DataFrame(columns=["id", "count"])
        table["id"] = keys
        table["count"] = doc_counts
        table = table.sort_values(by='count', ascending=False)
        return table.to_json(orient='split')

    # FILTER DATA: Date
    def _filter_date(self, dimension, selected_dimensions, filter_options,
                     request_options):
        # extract filter_dimensions
        filter_dimensions = [filter_opt['id'] for filter_opt in filter_options]
        # dimension, selected dimensions and filter dimensions must exist
        query_dimensions = self.query.dimensions(
            selected_dimensions + filter_dimensions)
        is_filter_data = True
        query_filter = self.query.filter(is_filter_data, dimension,
                                         filter_options)
        interval = request_options['interval']
        query = {
            "query": {
                "bool": {
                    "must": query_dimensions,
                    "filter": {
                        "bool": {
                            "must": query_filter
                        }
                    }
                },
            },
            "size": 0,
            "aggs": {
                "values": {
                    "date_histogram": {
                        "field": dimension,
                        "interval": interval
                    }
                }
            }
        }
        date_histogram_buckets = self.es.query(
            self.es.idx_data, query)['aggregations']['values']['buckets']
        keys = [el["key"] for el in date_histogram_buckets]
        doc_counts = [el["doc_count"] for el in date_histogram_buckets]
        table = pd.DataFrame(columns=["key", "count"])
        table["key"] = keys
        table["count"] = doc_counts
        return table.to_json(orient='split')

    # FILTER DATA: Numeric
    def _filter_numeric(self, dimension, selected_dimensions, filter_options,
                        request_options):
        # extract filter_dimensions
        filter_dimensions = [filter_opt['id'] for filter_opt in filter_options]
        # get type of dimension
        dimension_type = self.init_dimensions_table.loc[
            self.init_dimensions_table.id == dimension].type.values[0]
        # dimension, selected dimensions and filter dimensions must exist
        query_dimensions = self.query.dimensions(
            selected_dimensions + filter_dimensions)
        # is filter dimension, but only want to see min max window ...
        # ... adjusted by user
        is_filter_data = False
        if "undo" in request_options:
            is_filter_data = True
        if "update" in request_options:
            filter_option = {
                "id": dimension,
                "type": dimension_type,
                "config": {
                    "min": request_options["min"],
                    "max": request_options["max"]
                }
            }
            filter_options = filter_options + [filter_option]
        query_filter = self.query.filter(is_filter_data, dimension,
                                         filter_options)

        query_range = {
            "query": {
                "bool": {
                    "must": query_dimensions,
                    "filter": {
                        "bool": {
                            "must": query_filter
                        }
                    }
                },
            },
            "size": 0,
            "aggs": {
                "max": {
                    "max": {
                        "field": dimension
                    }
                },
                "min": {
                    "min": {
                        "field": dimension
                    }
                }
            }
        }
        value_range = self.es.query(self.es.idx_data,
                                    query_range)['aggregations']
        value_min = value_range['min']['value']
        value_max = value_range['max']['value']

        interval = (value_max - value_min) / 100.0
        if dimension_type == "Integer":
            interval = int(interval)
            if interval < 1:
                interval = 1
        query = {
            "query": {
                "bool": {
                    "must": query_dimensions,
                    "filter": {
                        "bool": {
                            "must": query_filter
                        }
                    }
                },
            },
            "size": 0,
            "aggs": {
                "values": {
                    "histogram": {
                        "field": dimension,
                        "interval": interval
                    }
                }
            }
        }
        numeric_histogram_buckets = self.es.query(
            self.es.idx_data, query)['aggregations']['values']['buckets']
        keys = [el["key"] for el in numeric_histogram_buckets]
        doc_counts = [el["doc_count"] for el in numeric_histogram_buckets]
        table = pd.DataFrame(columns=["key", "count"])
        table["key"] = keys
        table["count"] = doc_counts
        return table.to_json(orient='split')

    def filter_data(self, dimension, selected_dimensions, filter_options,
                    request_options):
        dimension_type = self.init_dimensions_table.loc[
            self.init_dimensions_table.id == dimension].type.values[0]
        print("id:", dimension, " has type", dimension_type)
        filter_map = {
            "Text": self._filter_categorial,
            "Character": self._filter_categorial,
            "Identifier": self._filter_categorial,
            "Date": self._filter_date,
            "Integer": self._filter_numeric,
            "Double": self._filter_numeric
        }

        data = filter_map[dimension_type](dimension, selected_dimensions,
                                          filter_options, request_options)

        return data

    # query data:
    # - 1. geo vs double
    # - 2. double vs double
    def plot_data_dummy(self):
        # geo vs double
        plot_dimensions = ["spatiotempcovera0", "satellite_altitu0"]
        query_dimensions = self.query.dimensions(plot_dimensions)
        query = {
            "query": {
                "bool": {
                    "must": query_dimensions
                },
            },
            "size": 9999,
            "_source": plot_dimensions
        }
        data_query = self.es.query(self.es.idx_data, query)['hits']['hits']
        key1 = [r['_source']['spatiotempcovera0'] for r in data_query]
        key2 = [r['_source']['satellite_altitu0'] for r in data_query]
        table1 = pd.DataFrame(
            columns=["spatiotempcovera0", "satellite_altitu0"])
        table1["spatiotempcovera0"] = key1
        table1["satellite_altitu0"] = key2
        table1_json = table1.to_json(orient='split')

        # double vs double
        plot_dimensions = ["satellite_headin0", "satellite_altitu0"]
        query_dimensions = self.query.dimensions(plot_dimensions)
        query = {
            "query": {
                "bool": {
                    "must": query_dimensions
                },
            },
            "size": 9999,
            "_source": plot_dimensions
        }
        data_query = self.es.query(self.es.idx_data, query)['hits']['hits']
        key1 = [r['_source']['satellite_headin0'] for r in data_query]
        key2 = [r['_source']['satellite_altitu0'] for r in data_query]
        table2 = pd.DataFrame(
            columns=["satellite_headin0", "satellite_altitu0"])
        table2["satellite_headin0"] = key1
        table2["satellite_altitu0"] = key2
        table2_json = table2.to_json(orient='split')

        plot_data = [table1_json, table2_json]
        return json.dumps(plot_data)

    def plot_map_heatmap(self, bounds, dimension, selected_dimensions,
                         filter_dimensions):
        # function for cartesian product
        def cartesian(lists):
            if len(lists) == 0:
                return [()]
            return [
                x + (y, ) for x in cartesian(lists[:-1]) for y in lists[-1]
            ]

        # class for encoding numpt arrays
        class NumpyEncoder(json.JSONEncoder):
            def default(self, obj):
                if isinstance(obj, np.ndarray):
                    return obj.tolist()
                if isinstance(obj, np.int64): return int(obj)
                return json.JSONEncoder.default(self, obj)

        num_x = 64
        num_y = 36
        grid_x = np.linspace(bounds[0][0], bounds[1][0], num_x)
        grid_y = np.linspace(bounds[0][1], bounds[1][1], num_y)
        upperleft = cartesian([grid_x[:-1], grid_y[:-1]])
        lowerright = cartesian([grid_x[1:], grid_y[1:]])
        middle_points = list()
        query_arr = list()
        query_dimensions = self.query.dimensions(
            selected_dimensions + filter_dimensions)
        for i in range(len(upperleft)):
            middle = np.divide(np.add(upperleft[i], lowerright[i]), 2)
            middle = middle[-1::-1]
            middle_points.append(middle)
            query_geo = self.query.filter_geo(dimension, 'envelope',
                                              'INTERSECTS',
                                              (upperleft[i], lowerright[i]))
            query = {
                "query": {
                    "bool": {
                        "must": query_dimensions,
                        "filter": {
                            "bool": {
                                "must": query_geo
                            },
                        }
                    }
                },
                'size': 0
            }
            query_arr.append({'index': self.es.idx_data})
            query_arr.append(query)
        request = ''
        for each in query_arr:
            request += '%s \n' % json.dumps(each)
        resp = self.es.mquery(query_arr)
        geo_counts = [r['hits']['total'] for r in resp['responses']]
        geo_counts = np.array(geo_counts).reshape(len(geo_counts), 1)
        idx = np.nonzero(geo_counts)
        geo_max = np.max(geo_counts)
        middle_points = np.array(middle_points)
        if geo_max != 0:
            geo_counts = geo_counts / geo_max
        data = np.concatenate((middle_points, geo_counts), axis=-1)
        return json.dumps({'table': data, 'max': geo_max}, cls=NumpyEncoder)
