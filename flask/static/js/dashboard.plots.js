// =============================================================================
// PLOTS
// =============================================================================
$.fn.extend({
  plot: {
    heatmap: function() {
      var heatmap = function(map) {
        console.log("Ready.");
        $(this).event.progress(true);
        $.ajax({
          type: 'POST',
          url: "/_plot_heatmap",
          data: JSON.stringify({
            bounds: map.getBounds()
          }),
          dataType: 'json',
          contentType: 'application/json',
          timeout: global.config.ajax_timeout
        }).done(function(e) {
          var data = JSON.parse(JSON.stringify(e));
          var heatData = data.table;
          var heatMax = data.max;
          if (global.plots.layer !== undefined) {
            map.removeLayer(global.plots.layer);
          }
          if (global.plots.legend !== undefined) {
            global.plots.legend.remove(map);
          }
          var heatLayer = L.idwLayer(heatData, {
            opacity: 0.4,
            maxZoom: map.getZoom(),
            cellSize: 20,
            exp: 2,
            gradient: {
              0.01: $(this).plot.helper.getColor(0.01),
              0.05: $(this).plot.helper.getColor(0.05),
              0.1: $(this).plot.helper.getColor(0.1),
              0.5: $(this).plot.helper.getColor(0.5),
              0.75: $(this).plot.helper.getColor(0.75),
              1: $(this).plot.helper.getColor(1)
            }
          });
          heatLayer.addTo(map);
          global.plots.layer = heatLayer;
          var legend = L.control({
            position: 'bottomright'
          });
          legend.onAdd = function(map) {
            var div = L.DomUtil.create('div', 'info legend'),
              grades = [0.01, 0.05, 0.1, 0.5, 0.75, 1],
              labels = [],
              from;

            for (var i = 1; i < grades.length; i++) {
              from = grades[i];
              grades[i]
              labels.push(
                '<div><i style="background:' + $(this).plot.helper.getColor(from) + '"></i><p>' + (from * heatMax).toFixed(0) + '</p</div>');
            }
            div.innerHTML = labels.join('');
            return div;
          };
          global.plots.legend = legend.addTo(map);
          $(this).event.progress(false);
        }).fail(function() {
          M.toast({
            html: 'Server: ' + parseInt(global.config.ajax_timeout / 1000) + ' seconds timeout exceeded',
            displayLength: 4000
          });
          $(this).event.progress(false);
          console.log("Failed.");
        });
      }
      if (global.plots.map !== undefined) {
        global.plots.map.remove();
      }
      var map = $(this).plot.helper.getMap('plot_dashboard_body', 5, [0, 0]);
      global.plots.map = map;
      map.on('dragend zoomend', function() {
        heatmap(map);
      });

      heatmap(map);
    },
    helper: {
      getMap: function(div, zoomLevel, center) {
        var map = L.map(div, {
          minZoom: 2,
          maxZoom: zoomLevel
        }).setView(center, 2);
        var southWest = L.latLng(-90, -180),
          northEast = L.latLng(90, 180);
        var bounds = L.latLngBounds(southWest, northEast);
        map.setMaxBounds(bounds);
        L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_all/{z}/{x}/{y}{r}.png', {
          attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>',
          maxZoom: 10,
          minZoom: 1,
          bounds: bounds,
          reuseTiles: true,
          noWrap: true
        }).addTo(map);
        map.on('drag', function() {
          map.panInsideBounds(bounds, {
            animate: false
          });
        });
        var ZoomViewer = L.Control.extend({
          onAdd: function() {
            var container = L.DomUtil.create('div');
            var gauge = L.DomUtil.create('div');
            container.style.width = '200px';
            container.style.background = 'rgba(255,255,255,0.5)';
            container.style.textAlign = 'left';
            map.on('zoomstart zoom zoomend', function(ev) {
              gauge.innerHTML = 'Zoom level: ' + map.getZoom();
            })
            container.appendChild(gauge);
            return container;
          }
        });
        (new ZoomViewer).addTo(map);
        return map;
      },
      getColor: function(val) {
        return val == 1 ? 'red' :
          val >= 0.75 ? 'orange' :
          val >= 0.5 ? 'yellow' :
          val >= 0.1 ? 'lime' :
          val >= 0.05 ? 'green' :
          'transparent';
      },
      getCol: function(matrix, col) {
        var column = [];
        for (var i = 0; i < matrix.length; i++) {
          column.push(matrix[i][col]);
        }
        return column;
      }
    },
    filter_data: {
      date: function(id_name, data, interval) {
        var date_format_map = {
          "year": "%Y",
          "month": "%b %Y",
          "day": "%d.%b %Y"
        };
        var date_format = date_format_map[interval];

        var plot_data = [{
          x: $(this).plot.helper.getCol(data, 0),
          y: $(this).plot.helper.getCol(data, 1),
          type: 'bar'
        }];

        var plot_layout = {
          xaxis: {
            title: 'Date',
            titlefont: {
              size: 16,
              color: 'rgb(107, 107, 107)'
            },
            tickfont: {
              size: 14,
              color: 'rgb(107, 107, 107)'
            },
            type: 'date'
          },
          yaxis: {
            title: 'Counts',
            titlefont: {
              size: 16,
              color: 'rgb(107, 107, 107)'
            },
            tickfont: {
              size: 14,
              color: 'rgb(107, 107, 107)'
            },
            fixedrange: true
          },
          bargap: 0.15,
          bargroupgap: 0.1,
          paper_bgcolor: "#FAFAFA",
          plot_bgcolor: "#FAFAFA",
          //autosize: true,
          margin: {
            l: 65,
            r: 65,
            b: 50,
            t: 20,
            pad: 5
          }
        };
        const plot_config = {
          displayModeBar: false
        };
        Plotly.newPlot('filter_data_date', plot_data, plot_layout, plot_config);
      },
      numeric: function(id_name, data) {
        var plot_data = [{
          x: $(this).plot.helper.getCol(data, 0),
          y: $(this).plot.helper.getCol(data, 1),
          type: 'bar'
        }];

        var plot_layout = {
          xaxis: {
            title: 'Value',
            titlefont: {
              size: 16,
              color: 'rgb(107, 107, 107)'
            },
            tickfont: {
              size: 14,
              color: 'rgb(107, 107, 107)'
            },
            fixedrange: true
          },
          yaxis: {
            title: 'Counts',
            titlefont: {
              size: 16,
              color: 'rgb(107, 107, 107)'
            },
            tickfont: {
              size: 14,
              color: 'rgb(107, 107, 107)'
            },
            fixedrange: true
          },
          bargap: 0.15,
          bargroupgap: 0.1,
          paper_bgcolor: "#FAFAFA",
          plot_bgcolor: "#FAFAFA",
          //autosize: true,
          margin: {
            l: 65,
            r: 65,
            b: 50,
            t: 20,
            pad: 5
          }
        };
        const plot_config = {
          displayModeBar: false
        };
        Plotly.newPlot('filter_data_numeric', plot_data, plot_layout, plot_config);
      }
    }
  }
});
