// =============================================================================
// EVENTS
// =============================================================================
$.fn.extend({
  /** progress bar handler
   * arg:
   * - true: add loading progress
   * - false: progress done
   **/
  event: {
    progress: function(arg) {
      var prog_bar = $(".progress div");
      var progs = global.progress.count;
      // count active progresses
      progs = arg ? progs + 1 : Math.max(progs - 1, 0);
      // active progress
      var new_class = "indeterminate";
      // inactive progress
      var old_class = "determinate";
      // swap class when all progresses done
      if (progs == 0) {
        [new_class, old_class] = [old_class, new_class];
      }
      prog_bar.removeClass(old_class).addClass(new_class);
      // update global progress count
      global.progress.count = progs;
    },
    konami: function() {
      // a key map of allowed keys
      var allowedKeys = {
        37: 'left',
        38: 'up',
        39: 'right',
        40: 'down',
        65: 'a',
        66: 'b'
      };
      // the 'official' Konami Code sequence
      var konamiCode = ['up', 'up', 'down', 'down', 'left', 'right', 'left', 'right', 'b', 'a'];
      // a variable to remember the 'position' the user has reached so far.
      var konamiCodePosition = 0;
      // add keydown event listener
      document.addEventListener('keydown', function(e) {
        // get the value of the key code from the key map
        var key = allowedKeys[e.keyCode];
        // get the value of the required key from the konami code
        var requiredKey = konamiCode[konamiCodePosition];
        // compare the key with the required key
        if (key == requiredKey) {
          // move to the next key in the konami code sequence
          konamiCodePosition++;
          // if the last key is reached, activate cheats
          if (konamiCodePosition == konamiCode.length) {
            konami_content();
            konamiCodePosition = 0;
          }
        } else {
          konamiCodePosition = 0;
        }
      });

      function konami_content() {
        // konami stuff
        $("#konami").remove();
        var content = '<iframe id="konami" style="z-index:9999;position:absolute;width:100%;height:100%;" src="https://www.youtube-nocookie.com/embed/19dD18sSA4E?rel=0&amp;controls=0&amp;showinfo=0;autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
        $("body").append(content).delay(7500).queue(function(next) {
          $("#konami").off().remove();
          next();
        });
      }
    }
  }
});