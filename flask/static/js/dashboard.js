// =============================================================================
// GLOBAL CONFIGURATION
// =============================================================================
var global = {
  config: {
    ajax_timeout: 100000
  },
  progress: {
    count: 0
  },
  dashboard: {
    selected_dimensions: [],
    filter_dimensions: [],
    dimensions_table: [],
    dimensions_types: [],
    filter_options: [],
    datasets: 0,
  },
  colorscheme: {
    select_dimensions: {
      filter: {
        listed: "green",
        unlisted: "grey"
      }
    }
  },
  standard: {
    month_names: ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ]
  },
  plots: {
    map: undefined,
    layer: undefined,
    legend: undefined
  }
};
// =============================================================================
// DASHBOARD
// =============================================================================
$(document).ready(function() {
  // =============================================================================
  // MATERIALIZE
  // =============================================================================
  $('ul.tabs').tabs();
  // TAB Color
  $(".tabs").css("background-color", "#FFF");
  // TAB Indicator/Underline Color
  $(".tabs>.indicator").css("background-color", '#111');
  // TAB Text Color
  $(".tabs>li>a").css("color", '#FFF');

  $('.tooltipped').tooltip();

  $('select').formSelect();

  $('#modal-custom-dashboard').modal({
    onOpenEnd: function() {
      $(this).api.select_dimensions();
    },
    onOpenStart: function() {
      $("#table-choose-dimension").fadeOut(0);
      var elem = $("ul.tabs");
      var instance = M.Tabs.getInstance(elem);
      instance.select('choose_dimension');
    }
  });

  $('.modal-footer .dropdown-trigger').dropdown({
    coverTrigger: false,
    onOpenStart: function(el) {
      $(".modal-footer").css("height", "250px");
      $(el).parent().parent().find("div").css("opacity", 0);
      $(el).parent().css("opacity", 1);
    },
    onCloseEnd: function(el) {
      $(".modal-footer").css("height", "54px");
      $(el).parent().parent().find("div").css("opacity", 1);
    },
  });

  // =============================================================================
  // CHOOSE dASHBOARD MODAL
  // =============================================================================
  // click on footer element
  $('#choose_modal_footer div div a').click(function() {
    if ($('span.new', this).length) {
      $('span', this).toggleClass("new");
    }
  });
  // dropdown selected dimensions
  $("#data_dropdown_selected_dimensions").on("click", "li", function() {
    var id_name_arr = $(this).attr('id').split("__");
    var id_name = id_name_arr[id_name_arr.length - 1];
    $(this).remove();
    global.dashboard.selected_dimensions = global.dashboard.selected_dimensions.filter(e => e !== id_name);
    // update badge of dropdown
    $("#dropdown_selected_dimensions span").html($("#data_dropdown_selected_dimensions li").length);
    // update tbody
    $(this).api.select_dimensions();
    M.toast({
      html: "Plot Dimensions: " + id_name + " removed",
      displayLength: 4000
    });
    $("#filter_data_body").fadeOut(0);
  });
  // on click dropdown selection dimensions & modal header tab_filter_data
  $("#tab_filter_data, #data_dropdown_selected_dimensions").click(function() {
    // remove options
    var select_el = $("#filter_data_select");
    select_el.children().not(":disabled").remove();
    // update options
    // selected dimensions
    for (id in global.dashboard.selected_dimensions) {
      var id_name = global.dashboard.selected_dimensions[id];
      select_el.append('<option class="filter_data_option_list__plot_dimension" value="' + id_name + '">Plot Dimensions | ' + id_name + '</option>');
    }
    // filter dimensions
    for (id in global.dashboard.filter_dimensions) {
      var id_name = global.dashboard.filter_dimensions[id];
      console.log("IDNAME:" + id_name);
      select_el.append('<option class="filter_data_option_list__filter_dimension" value="' + id_name + '">Filter Dimension | ' + id_name + '</option>');
    }
    // build new select
    select_el.val("default");
    select_el.formSelect();
    // clear filter data body
    $("#filter_data_body").html("");
    // do not display options
    $("#filter_data_options a").css("display", "none");

  });
  // filter data
  $("#data_dropdown_filter_data").on("click", "li", function() {
    var id_name_arr = $(this).attr('id').split("__");
    var id_name = id_name_arr[id_name_arr.length - 1];
    $(this).remove();
    global.dashboard.filter_options = global.dashboard.filter_options.filter(e => e.id !== id_name);
    // update badge of dropdown
    $("#dropdown_filter_data span").html($("#data_dropdown_filter_data li").length);

    M.toast({
      html: "Filter Options: " + id_name + " removed",
      displayLength: 4000
    });
    $("#tab_filter_data").trigger("click");
  });

  // select filter dimension
  $("#filter_data_select").on("change", function() {
    var id_name = $(this).val();
    var request_options = {
      "first_run": true
    };
    $(this).api.filter_data(id_name, request_options);
    // display options: save, undo, delete from list(only if selection is filter dimension)
    var filter_type = $(this).find('option:selected').text().split(" ")[0];

    $("#filter_data_options a").css("display", "inline-block");
    if (filter_type == "Plot") {
      $("#filter_data_options_remove").css("display", "none");
    }
  });

  $("#choose_modal_footer").click(function() {
    $(this).plot.filter_data.date();
  });

  // plot dashboard
  $("#plot_dashboard").click(function() {
    $(this).api.plot_data();
  });

  //
  $(window).resize(function() {
    var window_height = $(window).height();
    var nav_height = $("nav").height();
    var footer_height = $("footer").height();
    $("#plot_dashboard_body").css("height", window_height - nav_height - footer_height - 40);
  });
  $(window).trigger("resize");
  // add konami event
  $(this).event.konami();

  // dashboard layout
  $(this).api.dashboard_layout();

});