// =============================================================================
// API
// =============================================================================
$.fn.extend({
  api: {
    listener: {
      select_dimensions: function() {
        // click event plot dimension select
        $('#table-choose-dimension .choose_dimension_select').off().click(function() {
          var id = $(this).attr("id");
          var id_name_arr = id.split("__");
          var id_name = id_name_arr[id_name_arr.length - 1];
          if ($("#dropdown_selected_dimensions span").html() < 4) {
            // get number of datasets
            var datasets_number = $(this).parent().parent().find("td:nth-child(5)").html();
            // add to dashboard object
            global.dashboard.selected_dimensions.push(id_name);
            // update tbody
            $(this).api.select_dimensions();
            // reset filter
            $(".reset").trigger("click");
            // append to selected dimensions
            var new_li = '<li id="data_dropdown_selected_dimensions__' + id_name + '"><a href="#!">' + id_name + '<i class="material-icons">delete</i></a></li>';
            $("#data_dropdown_selected_dimensions").append(new_li);
            // update badge of dropdown
            $("#dropdown_selected_dimensions span").html($("#data_dropdown_selected_dimensions li").length);
            // update individual filter List
            global.dashboard.filter_dimensions = global.dashboard.filter_dimensions.filter(e => e !== id_name);
            M.toast({
              html: 'Plot Dimensions: ' + id_name + ' added<br>Number Datasets: ' + datasets_number,
              displayLength: 6000
            });
            if (!$('#dropdown_selected_dimensions span.new').length) {
              $('#dropdown_selected_dimensions span').toggleClass("new");
            }
          } else {
            M.toast({
              html: 'Plot Dimensions: Maximum of 4 Dimemsions reached',
              displayLength: 2500
            });
          }
        });
        // click event filter dimension select
        $('#table-choose-dimension .choose_dimension_filter').off().click(function() {
          var id = $(this).attr("id");
          var id_name_arr = id.split("__");
          var id_name = id_name_arr[id_name_arr.length - 1];
          var filter_class = "filter_listed";
          var color_listed = "green";
          var color_unlisted = "grey";
          if (!$(this).hasClass(filter_class)) {
            // add to dashboard object
            global.dashboard.filter_dimensions.push(id_name);
            M.toast({
              html: "Filter List: " + id_name + " added",
              displayLength: 4000
            });
            $(this).addClass(filter_class);
            $(this).removeClass(color_unlisted);
            $(this).addClass(color_listed);
          } else {
            $(this).removeClass(filter_class);
            $(this).removeClass(color_listed);
            $(this).addClass(color_unlisted);
            global.dashboard.filter_dimensions = global.dashboard.filter_dimensions.filter(e => e !== id_name);
            M.toast({
              html: "Filter List: " + id_name + " removed",
              displayLength: 4000
            });
          }
        });
      },
      filter_data: {
        numeric: function(id_name, data) {
          // save Filter Options
          $("#filter_data_options_save").off().click(function() {
            $("#filter_data_options_save").removeClass("red").addClass("grey");
            // proof if filter is updated or new
            var filter_option_existing = global.dashboard.filter_options.filter(e => e.id == id_name);
            var filter_is_new = (filter_option_existing.length == 0) ? true : false;
            // delete old Filter Options of id_name
            global.dashboard.filter_options = global.dashboard.filter_options.filter(e => e.id !== id_name);
            // get range
            var range_min = $("#filter_data_numeric_range_min").val();
            var range_max = $("#filter_data_numeric_range_max").val();

            // update Filter Optionss: only add if keywords array has values
            var id_type = global.dashboard.dimensions_types.find(x => x.id === id_name).type;
            var interval = $("#filter_data_interval_form").find(":checked").next('span').html();
            var filter_option = {
              id: id_name,
              type: id_type,
              config: {
                min: range_min,
                max: range_max
              }
            };

            console.log(filter_option);
            var non_empty_filter_option = true;
            if (non_empty_filter_option) {
              // append id to dropdown filter
              if (filter_is_new) {
                var new_li = '<li id="dropdown_filter_data__' + id_name + '"><a href="#!">' + id_name + '<i class="material-icons">delete</i></a></li>';
                $("#data_dropdown_filter_data").append(new_li);
                // update number of filters
                $("#dropdown_filter_data span").html($("#data_dropdown_filter_data li").length);
                // new filter sign
                if (!$('#dropdown_filter_data span.new').length) {
                  $('#dropdown_filter_data span').toggleClass("new");
                }
              }
              global.dashboard.filter_options.push(filter_option);
              // get number of datasets
              var number_datasets = 0;
              for (var i in data) {
                number_datasets += data[i][1];
              }
              // var number_datasets = 0;
              // for (var row in data_selected) {
              //   number_datasets += data_selected.y[row];
              // }
              M.toast({
                html: "Filter Options: " + id_name + " added<br>Number Datasets: " + number_datasets,
                displayLength: 4000
              });
            } else {
              // empty Filter Options -> delete dropdown element
              $("#dropdown_filter_data__" + id_name).remove();
              $("#dropdown_filter_data span").html($("#data_dropdown_filter_data li").length);

              $('#dropdown_filter_data span').removeClass("new");
              M.toast({
                html: "Filter Options: " + id_name + " has no constraints",
                displayLength: 6000
              });
            }

            $("#tab_filter_data").trigger("click");
          });
          // reset options
          $("#filter_data_options_undo").off().click(function() {
            $("#filter_data_options_save").removeClass("grey").addClass("red");
            var request_options = {
              "undo": true,
              "first_run": false
            };
            $(this).api.filter_data(id_name, request_options);
          });
          // update options
          function isNumber(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
          }
          var id_type = global.dashboard.dimensions_types.find(x => x.id === id_name).type;
          $("#filter_data_numeric_update").off().click(function() {
            // get range
            var range_min = $("#filter_data_numeric_range_min").val();
            var range_max = $("#filter_data_numeric_range_max").val();

            if (id_type == "Integer") {
              if (!(Math.floor(range_min) == range_min) || !$.isNumeric(range_min) || !(Math.floor(range_max) == range_max) || !$.isNumeric(range_max) || range_min >= range_max) {
                return;
              }
            } else {
              if (!$.isNumeric(range_min) || !$.isNumeric(range_max) || range_min >= range_max) {
                return;
              }
            }

            var request_options = {
              "first_run": false,
              "update": true,
              "min": range_min,
              "max": range_max,
            };
            $("#filter_data_options_save").removeClass("grey").addClass("red");
            $(this).api.filter_data(id_name, request_options);
          });
          // remove filter dimension from filter list
          $("#filter_data_options_remove").off().click(function() {
            global.dashboard.filter_dimensions = global.dashboard.filter_dimensions.filter(e => e !== id_name);
            global.dashboard.filter_options = global.dashboard.filter_options.filter(e => e.id !== id_name);
            $("#tab_filter_data").trigger("click");
            // remove active filter of id in table
            M.toast({
              html: "Filter List: " + id_name + " removed",
              displayLength: 4000
            });
            $("#choose_dimension_filter__" + id_name).removeClass(color_listed).addClass(color_unlisted).removeClass("filter_listed");
            // remove from dropdown filter
            $("#data_dropdown_filter_data li").filter(function(index) {
              return $(this).attr("id").split("__")[1] == id_name;
            }).remove();
            $("#dropdown_filter_data span").html($("#data_dropdown_filter_data li").length);
            $('#dropdown_filter_data span').removeClass("new");
          });
        },
        date: function(id_name, initial_gte, initial_lte, data) {
          // change group by option: year, month, day
          $("#filter_data_interval_form label").off().click(function() {
            var interval = $(this).find("span").html();
            var request_options = {
              "interval": interval
            };
            $(this).api.filter_data(id_name, request_options);
          });
          var relayout_event_handler = true;
          $("#filter_data_date").off().get(0).on('plotly_relayout',
            function(eventdata) {
              if (relayout_event_handler) {
                // interval
                var interval = $("#filter_data_interval_form").find(":checked").next('span').html();
                var date_min;
                var date_max;
                if (!(eventdata['xaxis.range[0]'] == null)) {
                  date_min = eventdata['xaxis.range[0]'];
                } else {
                  date_min = $("#filter_data_date").get(0).layout.xaxis.range[0];
                }
                if (!(eventdata['xaxis.range[1]'] == null)) {
                  date_max = eventdata['xaxis.range[1]'];
                } else {
                  date_max = $("#filter_data_date").get(0).layout.xaxis.range[1];
                }
                // round date
                $("#filter_data_options_save").removeClass("grey").addClass("red");
                // update date range
                var date_min_html = $(this).api.helper.filter_data_date_parse_html(date_min, interval);
                var date_max_html = $(this).api.helper.filter_data_date_parse_html(date_max, interval);
                var date_min_rounded = new Date(date_min_html).getTime();
                var date_max_rounded = new Date(date_max_html).getTime();
                $('#filter_data_date_range_min').html(date_min_html).data("date", date_min_rounded);
                $('#filter_data_date_range_max').html(date_max_html).data("date", date_max_rounded);
                // round date down to interval level
                //alert(new Date(date_min_html));
                if (date_min_rounded != date_min && date_max_rounded != date_max) {
                  var update = {
                    'xaxis.range': [date_min_rounded, date_max_rounded]
                  };
                  Plotly.relayout($("#filter_data_date")[0], update);
                  relayout_event_handler = false;
                }
              } else {
                relayout_event_handler = true;
              }
            });
          // save Filter Options
          $("#filter_data_options_save").off().click(function() {
            $("#filter_data_options_save").removeClass("red").addClass("grey");
            // proof if filter is updated or new
            var filter_option_existing = global.dashboard.filter_options.filter(e => e.id == id_name);
            var filter_is_new = (filter_option_existing.length == 0) ? true : false;
            // delete old Filter Options of id_name
            global.dashboard.filter_options = global.dashboard.filter_options.filter(e => e.id !== id_name);
            // get selected keywords
            var keywords = [];
            var counts = [];
            $(".filter_data_select").each(function() {
              if ($(this).data("listed")) {
                keywords.push($(this).data("value"));
                counts.push($(this).data("count"));
              }
            });
            // update Filter Optionss: only add if keywords array has values
            var id_type = global.dashboard.dimensions_types.find(x => x.id === id_name).type;
            var interval = $("#filter_data_interval_form").find(":checked").next('span').html();
            var filter_option = {
              id: id_name,
              type: id_type,
              config: {
                gte: $('#filter_data_date_range_min').data("date"),
                lte: $('#filter_data_date_range_max').data("date"),
                interval: interval
              }
            };
            console.log(filter_option);
            var non_empty_filter_option = true;
            if (filter_option.config.gte == initial_gte && filter_option.config.lte == initial_lte) {
              non_empty_filter_option = false;
            }
            if (non_empty_filter_option) {
              // append id to dropdown filter
              if (filter_is_new) {
                var new_li = '<li id="dropdown_filter_data__' + id_name + '"><a href="#!">' + id_name + '<i class="material-icons">delete</i></a></li>';
                $("#data_dropdown_filter_data").append(new_li);
                // update number of filters
                $("#dropdown_filter_data span").html($("#data_dropdown_filter_data li").length);
                // new filter sign
                if (!$('#dropdown_filter_data span.new').length) {
                  $('#dropdown_filter_data span').toggleClass("new");
                }
              }
              global.dashboard.filter_options.push(filter_option);
              // get number of datasets
              var number_datasets = 0;
              for (var i in data) {
                console.log(data[i][0] + " , " + Number(new Date(data[i][0])) + ", " + filter_option.config.gte + ", " + filter_option.config.lte);
                var date_i = Number(new Date(data[i][0]));
                if (date_i >= filter_option.config.gte && date_i < filter_option.config.lte) {
                  number_datasets += data[i][1];
                }
              }
              // var number_datasets = 0;
              // for (var row in data_selected) {
              //   number_datasets += data_selected.y[row];
              // }
              M.toast({
                html: "Filter Options: " + id_name + " added<br>Number Datasets: " + number_datasets,
                displayLength: 4000
              });
            } else {
              // empty Filter Options -> delete dropdown element
              $("#dropdown_filter_data__" + id_name).remove();
              $("#dropdown_filter_data span").html($("#data_dropdown_filter_data li").length);

              $('#dropdown_filter_data span').removeClass("new");
              M.toast({
                html: "Filter Options: " + id_name + " has no constraints",
                displayLength: 6000
              });
            }

            $("#tab_filter_data").trigger("click");
          });
          // reset options
          $("#filter_data_options_undo").off().click(function() {
            var update = {
              'xaxis.range': [initial_gte, initial_lte]
            };
            Plotly.relayout($('#filter_data_date')[0], update);
            var date_min = $("#filter_data_date").get(0).layout.xaxis.range[0];
            var date_max = $("#filter_data_date").get(0).layout.xaxis.range[1];
            // update date range
            var date_min_html = $(this).api.helper.filter_data_date_parse_html(date_min, interval);
            var date_max_html = $(this).api.helper.filter_data_date_parse_html(date_max, interval);
          });
          // remove filter dimension from filter list
          $("#filter_data_options_remove").off().click(function() {
            global.dashboard.filter_dimensions = global.dashboard.filter_dimensions.filter(e => e !== id_name);
            global.dashboard.filter_options = global.dashboard.filter_options.filter(e => e.id !== id_name);
            $("#tab_filter_data").trigger("click");
            // remove active filter of id in table
            M.toast({
              html: "Filter List: " + id_name + " removed",
              displayLength: 4000
            });
            $("#choose_dimension_filter__" + id_name).removeClass(color_listed).addClass(color_unlisted).removeClass("filter_listed");
            // remove from dropdown filter
            $("#data_dropdown_filter_data li").filter(function(index) {
              return $(this).attr("id").split("__")[1] == id_name;
            }).remove();
            $("#dropdown_filter_data span").html($("#data_dropdown_filter_data li").length);
            $('#dropdown_filter_data span').removeClass("new");
          });
        }
      }
    },
    helper: {
      plot_options_types_code: function(input) {
        var code = "";
        for (var i in input) {
          code += input[i];
        }
        return code;
      },
      plot_options: function(plot_dimensions) {
        var plot_options_map = ({
          single: {
            "GeoObject": ["GeoPolygon", "GeoHeatmap"],
            "Date": ["Histogram"],
            "Integer": ["Histogram"],
            "Double": ["Histogram"],
            "Character": ["Histogram"],
            "Identifier": ["Histogram"],
            "Text": ["Histogram"]
          },
          two: {
            // GEO:DATE
            "GeoObjectDate": ["GeoPolygonTemporalSlider"],
            // GEO:INTEGER
            "GeoObjectNumeric": ["GeoPolygonNumericColor"],
            // GEO:Geo
            "GeoObjectGeoObject": ["GeoPolygon", "GeoHeatmap"],
            // Date:Integer
            "DateInteger": ["Scatter", "Heatmap"]
          },
          init: function() {
            // mapping
            var mapping = {
              "DateGeoObject": "GeoObjectDate",
              "GeoObjectInteger": "GeoObjectNumeric",
              "IntegerGeoObject": "GeoObjectNumeric",
              "GeoObjectDouble": "GeoObjectNumeric",
              "DoubleGeoObject": "GeoObjectNumeric",
              "IntegerDate": "DateInteger"
            }
            for (var key in mapping) {
              this.two[key] = this.two[mapping[key]];
            }
            return this;
          }
        }).init();
        var plot_types = [];
        for (var i in plot_dimensions) {
          var id_name = plot_dimensions[i];
          var id_type = $(this).api.helper.id_type(id_name);
          plot_types.push(id_type);
        }
        var plot_options_arr = [];
        // case single dimension
        if (plot_dimensions.length == 1) {
          plot_options_arr.push.apply(plot_options_arr, plot_options_map.single[plot_types[0]]);
        } else if (plot_dimensions.length == 2) {
          var types_code = $(this).api.helper.plot_options_types_code(plot_types);
          plot_options_arr.push.apply(plot_options_arr, plot_options_map.two[types_code]);
        }
        return plot_options_arr;
      },
      id_type: function(id_name) {
        return global.dashboard.dimensions_types.find(x => x.id === id_name).type;
      },
      build_tablesorter: function(el) {
        var max_rows = 9999;
        if (!(el.hasClass('tablesorter'))) {
          el.tablesorter({
              theme: "materialize",
              widthFixed: false,
              widgets: ["filter", "zebra"],
              widgetOptions: {
                zebra: ["even", "odd"],
                filter_reset: ".reset",
              }
            })
            .tablesorterPager({
              size: max_rows,
              container: $(".ts-pager"),
              cssGoto: ".pagenum",
              removeRows: false,
              output: '{{startRow} - {endRow} / {filteredRows} ({totalRows})}'
            });
        }
      },
      build_select_dimensions: function(data) {
        var table = $("#table-choose-dimension");
        // build tablesorter
        $(this).api.helper.build_tablesorter(table);
        // remove previous activated info tooltips
        table.find(".tooltipped").tooltip("destroy");
        // build tbody
        var tbody = "";
        for (var row in data) {
          var id_name = data[row][0];
          tbody += "<tr><td>";
          // information about description
          tbody += '<a style="margin-left:3px;margin-right:3px;" id="choose_dimension_info_row__' + id_name + '" class="choose_dimension_info grey lighten-5 btn-flat tooltipped" data-position="top" data-tooltip="Description: ' + data[row][5] + '"><i class="small material-icons green-text darken-2">info_outline</i></a>';
          // add select/filter button
          tbody += '<a style="margin-left:3px;margin-right:3px;" id="choose_dimension_select__' + id_name + '" class="choose_dimension_select waves-effect waves-dark teal lighten-4 btn-small"><i class="small material-icons">add</i>Plot</a>';
          // "add to filter" after chosen at least one dimension
          if (global.dashboard.selected_dimensions.length > 0) {
            var color;
            var class_filter_listed;
            if ($.inArray(id_name, global.dashboard.filter_dimensions) != -1) {
              color = global.colorscheme.select_dimensions.filter.listed;
              class_filter_listed = "filter_listed";
            } else {
              color = global.colorscheme.select_dimensions.filter.unlisted;
              class_filter_listed = "";
            }
            tbody += '<a style="margin-left:3px;margin-right:3px;" id="choose_dimension_filter__' + id_name + '" class="choose_dimension_filter waves-effect waves-dark ' + color + ' lighten-1 btn-small ' + class_filter_listed + '"><i class="small material-icons">library_add</i>Filter</a>';
          }
          tbody += "</td>";
          for (var col in data[row]) {
            if (col != 5) {
              tbody += "<td>" + data[row][col] + "</td>";
            }
          }
          tbody += "</tr>";
        }
        // tbody into page
        $('#tbody-choose-dimension').html(tbody);
        // activate info tooltips
        table.find(".tooltipped").tooltip();
        // tablesorter update and fade in table
        table.trigger("update");
        $("#table-choose-dimension .tablesorter-header,#table-choose-dimension .tablesorter-filter-row td, #filter_data_body tfoot tr th").addClass("grey lighten-5");
        table.fadeIn(500);
        // remove filter dimension from filter list if not exist in table
        var delete_filter_dim = [];
        for (var id in global.dashboard.filter_dimensions) {
          var id_name = global.dashboard.filter_dimensions[id];
          var filter_id_in_table = false;
          if (global.dashboard.selected_dimensions.length > 0) {
            for (var row in global.dashboard.dimensions_table) {
              if (id_name == global.dashboard.dimensions_table[row][0]) {
                filter_id_in_table = true;
              }
            }
          }
          if (!filter_id_in_table) {
            delete_filter_dim.push(id_name);
          }
        }
        global.dashboard.filter_dimensions = global.dashboard.filter_dimensions.filter(function(el) {
          return delete_filter_dim.indexOf(el) < 0;
        });
        $("#tab_filter_data").trigger("click");
        // M.toast({
        //   html: "Plot Dimensions: Selection Table loaded",
        //   displayLength: 4000
        // });
        // activate listener
        $(this).api.listener.select_dimensions();
      },
      data2dimensiontype: function(data) {
        for (var row in data) {
          var dimension_type = {
            "id": data[row][0],
            "type": data[row][2]
          };
          global.dashboard.dimensions_types.push(dimension_type);
        }
      },
      build_filter_categorical: function(id_name, data) {
        $("#filter_data_body").find("*").off();
        $("#filter_data_body").find("*").remove();
        $("#filter_data_options_undo,#filter_data_options_remove,#filter_data_options_save").off();
        var filter_data_body = '\
                  <table id="filter_data_table" style="margin-bottom:48px;">\
                    <thead>\
                      <tr>\
                        <th>Select</th>\
                        <th>Value</th>\
                        <th>Count</th>\
                      </tr>\
                    </thead>\
                    <tfoot>\
                      <tr>\
                      <th>Select</th>\
                      <th>Value</th>\
                      <th>Count</th>\
                      </tr>\
                        <tr class="tablesorter-ignoreRow" style="display:none;">\
                        <th colspan="3" class="ts-pager form-horizontal">\
                          <button type="button" class="btn first"><i class="small material-icons">first_page</i></button>\
                          <button type="button" class="btn prev"><i class="small material-icons">navigate_before</i></button>\
                          <span class="pagedisplay"></span>\
                          <!-- this can be any element, including an input -->\
                          <button type="button" class="btn next"><i class="small material-icons">navigate_next</i></button>\
                          <button type="button" class="btn last"><i class="small material-icons">last_page</i></button>\
                          <select class="pagesize browser-default" title="Select page size">\
                            <option value="10">10</option>\
                            <option value="20">20</option>\
                            <option value="30">30</option>\
                            <option value="40">40</option>\
                            <option selected="selected" value="9999">9999</option>\
                          </select>\
                          <select class="pagenum browser-default" title="Select page number"></select>\
                        </th>\
                      </tr>\
                    </tfoot>\
                    <tbody id="filter_data_tbody">\
                    </tbody>\
                  </table>';
        $("#filter_data_body").html(filter_data_body);
        // add select, id, count to tbody
        var tbody = "";
        for (var row in data) {
          var id_value = data[row][0];
          var id_count = data[row][1];
          tbody += "<tr>";
          tbody += '<td><a style="margin-left:3px;margin-right:3px;" data-value="' + id_value + '" data-count="' + id_count + '" data-listed=false class="filter_data_select waves-effect waves-dark grey btn-small"><i class="small material-icons">add</i>Add</a></td>';
          for (var col in data[row]) {
            tbody += "<td>" + data[row][col] + "</td>";
          }
          tbody += "</tr>";
          if (row >= 998) {
            M.toast({
              html: 'Filter Options: maximal numbers of 999 displayed unique values reached ',
              displayLength: 6000
            });
          }
        }
        $("#filter_data_tbody").html(tbody);
        $(this).api.helper.build_tablesorter($('#filter_data_body table'));
        $("#filter_data_body .tablesorter-header,#filter_data_body .tablesorter-filter-row td, #filter_data_body tfoot tr th").addClass("grey lighten-5");
        // colors
        var color_listed = "green";
        var color_unlisted = "grey";
        // colorize items selected in Filter Options
        var filter_option_existing = global.dashboard.filter_options.filter(e => e.id == id_name);
        var filter_is_new = true;
        if (filter_option_existing.length > 0) {
          filter_is_new = false;
          var existing_keywords = filter_option_existing[0].config.keywords;
          for (var i in existing_keywords) {
            var keyword = existing_keywords[i];
            $(".filter_data_select").each(function() {
              if ($(this).data("value") == keyword) {
                $(this).data("listed", true);
                $(this).removeClass(color_unlisted);
                $(this).addClass(color_listed);
              }
            })
          }
        }

        // add value
        $('#filter_data_body table').on("click", '.filter_data_select', function() {
          var id_value = $(this).data("value");
          if ($(this).data("listed")) {
            $(this).data("listed", false);
            $(this).removeClass(color_listed);
            $(this).addClass(color_unlisted);
          } else {
            $(this).data("listed", true);
            $(this).removeClass(color_unlisted);
            $(this).addClass(color_listed);
          }

          $("#filter_data_options_save").removeClass("grey").addClass("red");
        });
        // reset options
        $("#filter_data_options_undo").off().click(function() {
          var select_items = $("#filter_data_body table .filter_data_select");
          select_items.data("listed", false);
          select_items.removeClass(color_listed);
          select_items.addClass(color_unlisted);
          $("#filter_data_options_save").removeClass("grey").addClass("red");
        });
        // remove filter dimension from filter list
        $("#filter_data_options_remove").off().click(function() {
          global.dashboard.filter_dimensions = global.dashboard.filter_dimensions.filter(e => e !== id_name);
          global.dashboard.filter_options = global.dashboard.filter_options.filter(e => e.id !== id_name);
          $("#tab_filter_data").trigger("click");
          // remove active filter of id in table
          M.toast({
            html: "Filter List: " + id_name + " removed",
            displayLength: 4000
          });
          $("#choose_dimension_filter__" + id_name).removeClass(color_listed).addClass(color_unlisted).removeClass("filter_listed");
          // remove from dropdown filter
          $("#data_dropdown_filter_data li").filter(function(index) {
            return $(this).attr("id").split("__")[1] == id_name;
          }).remove();
          $("#dropdown_filter_data span").html($("#data_dropdown_filter_data li").length);
          $('#dropdown_filter_data span').removeClass("new");
        });
        // save Filter Options
        $("#filter_data_options_save").off().click(function() {
          $("#filter_data_options_save").removeClass("red").addClass("grey");
          // proof if filter is updated or new
          var filter_option_existing = global.dashboard.filter_options.filter(e => e.id == id_name);
          var filter_is_new = (filter_option_existing.length == 0) ? true : false;
          // delete old Filter Options of id_name
          global.dashboard.filter_options = global.dashboard.filter_options.filter(e => e.id !== id_name);
          // get selected keywords
          var keywords = [];
          var counts = [];
          $(".filter_data_select").each(function() {
            if ($(this).data("listed")) {
              keywords.push($(this).data("value"));
              counts.push($(this).data("count"));
            }
          });
          // update Filter Optionss: only add if keywords array has values
          var id_type = global.dashboard.dimensions_types.find(x => x.id === id_name).type;
          var filter_option = {
            id: id_name,
            type: id_type,
            config: {
              keywords: keywords
            }
          };
          if (keywords.length > 0) {
            // append id to dropdown filter
            if (filter_is_new) {
              var new_li = '<li id="dropdown_filter_data__' + id_name + '"><a href="#!">' + id_name + '<i class="material-icons">delete</i></a></li>';
              $("#data_dropdown_filter_data").append(new_li);
              // update number of filters
              $("#dropdown_filter_data span").html($("#data_dropdown_filter_data li").length);
              // new filter sign
              if (!$('#dropdown_filter_data span.new').length) {
                $('#dropdown_filter_data span').toggleClass("new");
              }
            }
            global.dashboard.filter_options.push(filter_option);
            var number_datasets = counts.reduce(function(pv, cv) {
              return pv + cv;
            }, 0);
            M.toast({
              html: "Filter Options: " + id_name + " added<br>Number Datasets: " + number_datasets,
              displayLength: 4000
            });

          } else {
            // empty Filter Options -> delete dropdown element
            $("#dropdown_filter_data__" + id_name).remove();
            $("#dropdown_filter_data span").html($("#data_dropdown_filter_data li").length);

            $('#dropdown_filter_data span').removeClass("new");
            M.toast({
              html: "Filter Options: " + id_name + " has no constraints",
              displayLength: 6000
            });
          }
          $("#tab_filter_data").trigger("click");
        });
        // body fade in
        $("#filter_data_body").fadeIn(500);
        console.log("Build filter data body done.");
      },
      filter_data_date_parse_html: function(date_value, interval) {
        var date = new Date(date_value);
        var date_span;
        if (interval == "day") {
          date_span = date.getFullYear() + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + ("0" + date.getDate()).slice(-2);
        } else if (interval == "month") {
          date_span = date.getFullYear() + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + "01";
        } else if (interval == "year") {
          date_span = date.getFullYear() + "-" + "01" + "-" + "01";
        }
        return date_span;
      },
      filter_data_date_xrange_adjust: function(xrange) {
        var xrange_min = xrange[0];
        var xrange_max = new Date(xrange[1]);
        xrange_max.setFullYear(xrange_max.getFullYear() + 1);
        xrange_max.setMonth(0);
        xrange_max.setDate(1);
        return [xrange_min, xrange_max.getTime()]
      },
      build_filter_date: function(id_name, data, request_options) {
        // check for existing filter
        var filter_option_existing = global.dashboard.filter_options.filter(e => e.id == id_name);
        if (filter_option_existing.length > 0) {
          var option_interval = filter_option_existing[0].config.interval;
          if (request_options.interval != option_interval && request_options.hasOwnProperty("first_run")) {
            var request_options = {
              "interval": option_interval
            };
            $(this).api.filter_data(id_name, request_options);
            return;
          }
        }
        $("#filter_data_body").find("*").off();
        $("#filter_data_body").find("*").remove();
        var interval = request_options.interval;
        // build content divs
        var content = '\
        <div class="row" style="margin-bottom:0px;">\
          <div class="col s6">\
            <i style="margin-right:10px;" class="material-icons">date_range</i>\
            <span id="filter_data_date_range_min"></span>\
            <i class="material-icons">chevron_right</i>\
            <span id="filter_data_date_range_max"></span>\
          </div>\
          <div class="col s6 right-align">\
            <form id="filter_data_interval_form" style="margin:0;padding:0;" action="#">\
              <label>\
                <input name="interval" type="radio" />\
                <span>year</span>\
              </label>\
              <label>\
                <input name="interval" type="radio" />\
                <span>month</span>\
              </label>\
              <label>\
                <input name="interval" type="radio" />\
                <span>day</span>\
              </label>\
            </form>\
          </div>\
          </div><div id="filter_data_date"></div>';
        $("#filter_data_body").html(content);
        $("#filter_data_body form label").each(function() {
          if ($(this).find("span").html() == interval) {
            $(this).find("input").attr('checked', true);
          } else {
            $(this).find("input").attr('checked', false);
          }
        });
        var modal_height = $("#modal-custom-dashboard").height();
        var modal_width = $("#modal-custom-dashboard").width();
        var plot_div = $("#filter_data_date");
        plot_div.css("width", modal_width);
        plot_div.css("height", modal_height - 250);
        // $('#filter_data_date_start').datepicker({
        //   container: 'body'
        // });
        $(this).plot.filter_data.date(id_name, data, interval);
        var range_initial = $(this).api.helper.filter_data_date_xrange_adjust(plot_div.get(0).layout.xaxis.range);

        // zoom in, if filter option activated
        var filter_option_existing = global.dashboard.filter_options.filter(e => e.id == id_name);
        if (filter_option_existing.length > 0) {
          var option_gte = filter_option_existing[0].config.gte;
          var option_lte = filter_option_existing[0].config.lte;
          var update = {
            'xaxis.range': [option_gte, option_lte]
          };
          Plotly.relayout(plot_div[0], update);
          date_min = plot_div.get(0).layout.xaxis.range[0];
          date_max = plot_div.get(0).layout.xaxis.range[1];
          // update date range
          var date_min_html = $(this).api.helper.filter_data_date_parse_html(date_min, interval);
          var date_max_html = $(this).api.helper.filter_data_date_parse_html(date_max, interval);
          $('#filter_data_date_range_min').html(date_min_html).data('date', date_min);
          $('#filter_data_date_range_max').html(date_max_html).data('date', date_max);
        } else {
          date_min = plot_div.get(0).layout.xaxis.range[0];
          date_max = plot_div.get(0).layout.xaxis.range[1];
          var update = {
            'xaxis.range': $(this).api.helper.filter_data_date_xrange_adjust([date_min, date_max])
          };
          Plotly.relayout(plot_div[0], update);
          date_min = plot_div.get(0).layout.xaxis.range[0];
          date_max = plot_div.get(0).layout.xaxis.range[1];
          var date_min_html = $(this).api.helper.filter_data_date_parse_html(date_min, interval);
          var date_max_html = $(this).api.helper.filter_data_date_parse_html(date_max, interval);
          $('#filter_data_date_range_min').html(date_min_html).data('date', date_min);
          $('#filter_data_date_range_max').html(date_max_html).data('date', date_max);
        }
        $(this).api.listener.filter_data.date(id_name, range_initial[0], range_initial[1], data);
        $("#filter_data_body").fadeIn(500);
      },
      build_filter_numeric: function(id_name, data, request_options) {
        $("#filter_data_body").find("*").off();
        $("#filter_data_body").find("*").remove();
        var interval = request_options.interval;
        var content = '\
        <div class="row" style="margin-bottom:0px;">\
          <div class="col s6">\
            <div class="input-field col s3">\
              <input id="filter_data_numeric_range_min" type="number" step=any class="validate">\
              <label for="filter_data_numeric_range_min">Min. Value</label>\
            </div>\
            <div class="input-field col s3">\
              <input id="filter_data_numeric_range_max" type="number" step=any class="validate">\
              <label for="filter_data_numeric_range_max">Max. Value</label>\
            </div>\
          <button id="filter_data_numeric_update" style="margin-left:15px;margin-top:20px;" type="button" class="btn-small first waves-effect waves-dark"><i class="small material-icons">autorenew</i>Update</button>\
          </div>\
          <div class="col s6 right-align">\
          </div>\
          </div><div id="filter_data_numeric"></div>';
        $("#filter_data_body").html(content);

        var modal_height = $("#modal-custom-dashboard").height();
        var modal_width = $("#modal-custom-dashboard").width();
        var plot_div = $("#filter_data_numeric");
        plot_div.css("width", modal_width);
        plot_div.css("height", modal_height - 300);
        $(this).plot.filter_data.numeric(id_name, data);

        // update input fields
        var plot_data = plot_div.get(0).data;
        var plot_data_x = plot_data[0].x;
        var plot_range_min = Math.min(...plot_data_x);
        var plot_range_max = Math.max(...plot_data_x);
        $("#filter_data_numeric_range_min").val(plot_range_min);
        $("#filter_data_numeric_range_max").val(plot_range_max);
        M.updateTextFields();

        $(this).api.listener.filter_data.numeric(id_name, data);

        $("#filter_data_body").fadeIn(500);
      }
    },
    select_dimensions: function() {
      console.log("Loading select dimensions table...");
      $(this).event.progress(true);
      $('#table-choose-dimension').fadeOut(0);
      $.ajax({
        type: 'POST',
        url: "/_select_dimensions",
        data: JSON.stringify({
          "selected_dimensions": global.dashboard.selected_dimensions
        }),
        dataType: 'json',
        contentType: 'application/json',
        timeout: global.config.ajax_timeout
      }).done(function(e) {
        var table = JSON.parse(JSON.stringify(e));
        var data = table.data;
        // save dimension types
        if (global.dashboard.dimensions_types.length == 0) {
          $(this).api.helper.data2dimensiontype(data);
        }
        // recent dimensions_select table
        global.dashboard.dimensions_table = data;
        $(this).api.helper.build_select_dimensions(data);
        $(this).event.progress(false);
        console.log("Done.");
      }).fail(function() {
        M.toast({
          html: 'Server: ' + parseInt(global.config.ajax_timeout / 1000) + ' seconds timeout exceeded',
          displayLength: 4000
        });
        $(this).event.progress(false);
      });
    },
    filter_data: function(id_name, request_options) {
      $(this).event.progress(true);
      var id_type = global.dashboard.dimensions_types.find(x => x.id === id_name).type;
      console.log("Loading filter data for id " + id_name + " with type " + id_type + " ...");
      if (request_options.first_run) {
        switch (id_type) {
          case "Date":
            request_options['interval'] = "year";
            break;
          case "Integer":
            var filter_option_existing = global.dashboard.filter_options.filter(e => e.id == id_name);
            if (filter_option_existing.length > 0) {
              var config = filter_option_existing[0].config;
              request_options['min'] = config.min;
              request_options['max'] = config.max;
              request_options.first_run = false;
            }
            break;
          case "Double":
            var filter_option_existing = global.dashboard.filter_options.filter(e => e.id == id_name);
            if (filter_option_existing.length > 0) {
              var config = filter_option_existing[0].config;
              request_options['min'] = config.min;
              request_options['max'] = config.max;
              request_options.first_run = false;
            }
            break;
        }
      }
      console.log("Sending to server: " + JSON.stringify({
        "dimension": id_name,
        "selected_dimensions": global.dashboard.selected_dimensions,
        "filter_options": global.dashboard.filter_options,
        "request_options": request_options
      }));
      $("#filter_data_body").fadeOut(0);
      $.ajax({
        type: 'POST',
        url: "/_filter_data",
        data: JSON.stringify({
          "dimension": id_name,
          "selected_dimensions": global.dashboard.selected_dimensions,
          "filter_options": global.dashboard.filter_options,
          "request_options": request_options
        }),
        dataType: 'json',
        contentType: 'application/json',
        timeout: global.config.ajax_timeout
      }).done(function(e) {
        var table = JSON.parse(JSON.stringify(e));
        var data = table.data;
        switch (id_type) {
          case "Text":
            $(this).api.helper.build_filter_categorical(id_name, data);
            break;
          case "Character":
            $(this).api.helper.build_filter_categorical(id_name, data);
            break;
          case "Identifier":
            $(this).api.helper.build_filter_categorical(id_name, data);
            break;
          case "Date":
            $(this).api.helper.build_filter_date(id_name, data, request_options);
            break;
          case "Integer":
            $(this).api.helper.build_filter_numeric(id_name, data, request_options);
            break;
          case "Double":
            $(this).api.helper.build_filter_numeric(id_name, data, request_options);
            break;
        }
        $(this).event.progress(false);
      }).fail(function() {
        M.toast({
          html: 'Server: ' + parseInt(global.config.ajax_timeout / 1000) + ' seconds timeout exceeded',
          displayLength: 4000
        });
        $(this).event.progress(false);
        console.log("Failed.");
      });
    },
    plot_data: function() {
      $(this).plot.heatmap();

    },
    dashboard_layout: function() {
      // tab plot layout
      // on click dropdown selection dimensions & modal header tab_filter_data
      $("#tab_dashboard_layout").off().click(function() {
        var plot_dimensions = global.dashboard.selected_dimensions;
        if (plot_dimensions.length == 0) {
          M.toast({
            html: 'Please select at least one plot dimension',
            displayLength: 4000
          });
        } else {
          var el = $("#dashboard_layout_select_dimensions");
          el.children().not(":disabled").remove();
          // plot dimensions
          for (id in plot_dimensions) {
            var id_name = plot_dimensions[id];
            var id_type = global.dashboard.dimensions_types.find(x => x.id === id_name).type;
            el.append('<option class="dashboard_layout_select_dimensions_option" value="' + id_name + '" data-id_type="' + id_type + '">' + id_name + ' (' + id_type + ')</option>');
          }
          // build new select
          el.val("default");
          el.formSelect();
        }
      });
      $("#dashboard_layout_get_plot_options").off().click(function() {
        // get selected plot dimensions
        var plot_dimensions = $("#dashboard_layout_select_dimensions").val();
        if (plot_dimensions.length > 2) {
          M.toast({
            html: 'Choose maximal two dimensions for the plot',
            displayLength: 4000
          });
        } else if (plot_dimensions.length == 0) {
          M.toast({
            html: 'Choose at least one dimension for the plot',
            displayLength: 4000
          });
        } else {
          var plot_options = $(this).api.helper.plot_options(plot_dimensions);
          var el = $("#dashboard_layout_plot_options");
          el.children().not(":disabled").remove();
          // plot dimensions
          for (id in plot_options) {
            var option = plot_options[id];
            el.append('<option class="dashboard_layout_plot_options_option" value="' + option + '">' + option + '</option>');
          }
          // build new select
          el.val("default");
          el.formSelect();
        }
      });

      $("#dashboard_layout_select_dimensions").off().on("click", "option", function() {
        var el = $("#dashboard_layout_plot_options");
        el.children().not(":disabled").remove();
      });
    }
  }

});