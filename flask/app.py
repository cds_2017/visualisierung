# ========================================
# Visualisierung, FLASK APP
# Jonas Gütter, Paul Rudolph, Markus Unkel
# ========================================

# =========
# LIBRARIES
# =========

from flask import Flask, render_template, request, jsonify
from elasticsearch import Elasticsearch
import json
import pandas as pd
import os
from collections import OrderedDict
from fnmatch import filter as fnfilter
import time

dir_path = os.path.dirname(os.path.realpath(__file__))
project_path = os.path.dirname(dir_path)

import sys
sys.path.insert(0, project_path + '/elasticsearch')
sys.path.insert(0, project_path + '/data_cleansing')
from wrapper_es import WrapperES
from cleanse_data import DataCleanser

# ==============
# Import Modules
# ==============
import modules.api as api

# ==============
# FILE LOCATIONS
# ==============

config_path = project_path + '/vis.config'
dict_conf = dict()
dict_val = dict()
if os.path.exists(config_path):
    with open(config_path) as conf:
        file_string = conf.read()
        for line in file_string.split('#'):
            if len(line) != 0:
                line = line.replace("'", '').split('\n')
                key = line[0]
                value = line[1]
                [value1, value2] = value.split('=')
                dict_conf[key] = value2
                dict_val[key] = value1
else:
    dict_conf = {
        'Project Folder':
        project_path,
        'Data Folder':
        project_path + '/src/data',
        'Metadata Folder':
        project_path + '/src/meta',
        'Sensoren.tsv Folder':
        project_path + '/src/',
        'Temporary Folder':
        project_path + '/tmp',
        'Elasticsearch Install Folder':
        project_path + '/install/elasticsearch',
        'Elasticsearch Configuration Folder':
        project_path + '/elasticsearch/config'
    }
    dict_val = {
        'Project Folder': 'project_folder',
        'Data Folder': 'data_folder',
        'Metadata Folder': 'meta_folder',
        'Sensoren.tsv Folder': 'sensor_folder',
        'Temporary Folder': 'tmp_folder',
        'Elasticsearch Install Folder': 'elasticsearch_path',
        'Elasticsearch Configuration Folder': 'elasticsearch_config'
    }

    with open(config_path, 'w') as f:
        for key in dict_conf.keys():
            f.write("#{0}\n{1}='{2}'\n".format(key, dict_val[key],
                                               dict_conf[key]))

# =====
# FLASK
# =====

app = Flask(__name__)
wrapper = WrapperES()
api = api.API()


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/data', methods=['GET', 'POST'])
def data():
    # get file locations
    data_dir = dict_conf['Data Folder']
    sensor_file = dict_conf['Sensoren.tsv Folder'] + '/sensoren.tsv'
    meta_dir = dict_conf['Metadata Folder']
    tsv_files = fnfilter(os.listdir(data_dir), '*.tsv')
    tsv_names = [x.split('.')[0] for x in tsv_files]
    json_desc_file = dict_conf['Metadata Folder'] + '/_columnDescription.json'
    desc_json = json.load(open(json_desc_file))
    if request.method == 'POST':
        if request.form.get('Reset') == 'Reset':
            wrapper.reset_indicies(desc_json)
        if request.form.get('Upload') == 'Upload':
            wrapper.create_indicies(desc_json)
            sensor_data = pd.read_csv(sensor_file, sep='\t', encoding="UTF-16")
            wrapper.upload_sensoren_tsv(sensor_data)
            cc = DataCleanser(data_dir, meta_dir, desc_json)
            for x in tsv_names:
                cc.load_data(x)
                cc.do_cleansing()
                tsv_f = cc.tsv_file
                json_f = cc.json_file
                wrapper.upload_data(x, tsv_f, json_f)
                cc.save_changes(dict_conf['Temporary Folder'])
                wrapper.upload_column_desc(cc.column_desc)
    try:
        uploaded_data = api.uploaded_data()
        uploaded_data = OrderedDict(sorted(uploaded_data.items()))
    except:
        uploaded_data = OrderedDict()
    return render_template('data.html', files=uploaded_data)


@app.route('/config', methods=['GET', 'POST'])
def config():
    if request.method == 'POST':
        if request.form.get('Save') == 'Save':
            with open(config_path, 'w') as f:
                for key in dict_conf.keys():
                    dict_conf[key] = request.form.get(key)
                    f.write("#{0}\n{1}='{2}'\n".format(key, dict_val[key],
                                                       dict_conf[key]))
    return render_template('config.html', data=dict_conf)


@app.route('/_select_dimensions', methods=['POST'])
def _tbody_choose_dimension():
    selected_dimensions = request.get_json()['selected_dimensions']
    return api.plot_dimensions_table(selected_dimensions)


@app.route('/_filter_data', methods=['POST'])
def _filter_data():
    request_obj = request.get_json()
    dimension = request_obj['dimension']
    selected_dimensions = request_obj['selected_dimensions']
    filter_options = request_obj['filter_options']
    request_options = request_obj['request_options']
    return api.filter_data(dimension, selected_dimensions, filter_options,
                           request_options)


@app.route('/_plot_heatmap', methods=['POST'])
def _plot_heatmap():
    bounds = [[-180, -90], [180, 90]]
    dimension = 'spatiotempcovera0'
    selected_dimensions = ['spatiotempcovera0']
    filter_dimensions = []
    req = request.get_json()['bounds']
    bounds[0][0] = max(req['_southWest']['lng'], -180)
    bounds[0][1] = max(req['_southWest']['lat'], -90)
    bounds[1][0] = min(req['_northEast']['lng'], 180)
    bounds[1][1] = min(req['_northEast']['lat'], 90)
    return api.plot_map_heatmap(bounds, dimension, selected_dimensions, filter_dimensions)


@app.route('/_uploaded_files', methods=['POST'])
def uploaded_files():
    uploaded_data = api.uploaded_data()
    uploaded_data = OrderedDict(sorted(uploaded_data.items()))
    return json.dumps(uploaded_data)


if __name__ == '__main__':
    app.run(threaded=True, debug=True, port=8000)
