# ========================================
# Visualisierung, Setup Elasticsearch
# Jonas Gütter, Paul Rudolph, Markus Unkel
# ========================================

# =========
# LIBRARIES
# =========
from elasticsearch import Elasticsearch
from elasticsearch import helpers
from geomet import wkt
import pandas as pd
import json
from os import listdir, makedirs, path
from fnmatch import filter as fnfilter
from time import localtime, strftime


class WrapperES:
    def __init__(self, host='localhost', port=9200):
        self.es = Elasticsearch([{'host': host, 'port': port}], timeout=1000)
        # index setup
        self.idx_data = 'dlr_metadata'
        self.idx_desc = 'dlr_description'
        self.idx_file_names = 'dlr_file_names'
        self.doc_type_metadata = 'metadata'
        self.doc_type_description = 'description'
        self.doc_type_file_names = 'file_names'

    def create_indicies(self, column_desc):

        # Generate mappings
        dlr_metadata_body = {
            "settings": {
                "number_of_shards": 1,
                "number_of_replicas": 0,
                "codec": 'best_compression',
            },
            "mappings": {
                "metadata": {
                    "properties": {}
                }
            }
        }
        dlr_description_body = {
            "settings": {
                "number_of_shards": 1,
                "number_of_replicas": 0
            }
        }
        dlr_file_names = {
            "settings": {
                "number_of_shards": 1,
                "number_of_replicas": 0
            }
        }
        # Apply mapping to each column in dataset (type definitions).
        dict_type = {
            "GeoObject": {
                "type": "geo_shape",
                "tree": "quadtree",
                "precision": "10000m",
                "distance_error_pct": 0.01,
                "ignore_malformed": True
            },
            "Date": {
                "type": "date",
                "format": "yyyy-MM-dd HH:mm:ss.SSS",
            },
            "Double": {
                "type": "double",
            },
            "Integer": {
                "type": "integer",
            },
            "Character": {
                "type": "keyword",
            },
            "Identifier": {
                "type": "keyword",
            },
            "Boolean": {
                "type": "keyword",
            },
            "Text": {
                "type": "keyword",
                # "norms": 'false',
                # "index_options": "freqs"
            }
        }
        for i in range(len(column_desc)):
            if 'id' in column_desc[i]:
                col_type = column_desc[i]['type']
                dlr_metadata_body["mappings"]["metadata"]["properties"][
                    column_desc[i]['id']] = dict_type[col_type]

        if not self.es.indices.exists(index=self.idx_data):
            self.es.indices.create(
                index=self.idx_data, ignore=400, body=dlr_metadata_body)
        if not self.es.indices.exists(index=self.idx_desc):
            self.es.indices.create(
                index=self.idx_desc, body=dlr_description_body)
        if not self.es.indices.exists(index=self.idx_file_names):
            self.es.indices.create(
                index=self.idx_file_names, body=dlr_file_names)

    def reset_indicies(self, column_desc):

        # Generate mappings
        dlr_metadata_body = {
            "settings": {
                "number_of_shards": 1,
                "number_of_replicas": 0,
                "codec": 'best_compression',
            },
            "mappings": {
                "metadata": {
                    "properties": {}
                }
            }
        }
        dlr_description_body = {
            "settings": {
                "number_of_shards": 1,
                "number_of_replicas": 0
            }
        }
        dlr_file_names = {
            "settings": {
                "number_of_shards": 1,
                "number_of_replicas": 0
            }
        }
        # Apply mapping to each column in dataset (type definitions).
        dict_type = {
            "GeoObject": {
                "type": "geo_shape",
                "tree": "quadtree",
                "precision": "10000m",
                "distance_error_pct": 0.01,
                "ignore_malformed": True
            },
            "Date": {
                "type": "date",
                "format": "yyyy-MM-dd HH:mm:ss.SSS",
            },
            "Double": {
                "type": "double",
            },
            "Integer": {
                "type": "integer",
            },
            "Character": {
                "type": "keyword",
            },
            "Identifier": {
                "type": "keyword",
            },
            "Boolean": {
                "type": "keyword",
            },
            "Text": {
                "type": "keyword",
                # "norms": 'false',
                # "index_options": "freqs"
            }
        }
        for i in range(len(column_desc)):
            if 'id' in column_desc[i]:
                col_type = column_desc[i]['type']
                dlr_metadata_body["mappings"]["metadata"]["properties"][
                    column_desc[i]['id']] = dict_type[col_type]
        if self.es.indices.exists(index=self.idx_data):
            self.es.indices.delete(index=self.idx_data)
        self.es.indices.create(
            index=self.idx_data, ignore=400, body=dlr_metadata_body)
        if self.es.indices.exists(index=self.idx_desc):
            self.es.indices.delete(index=self.idx_desc)
        self.es.indices.create(index=self.idx_desc, body=dlr_description_body)
        if self.es.indices.exists(index=self.idx_file_names):
            self.es.indices.delete(index=self.idx_file_names)
        self.es.indices.create(index=self.idx_file_names, body=dlr_file_names)

    # upload column descriptions
    def upload_column_desc(self, column_desc):
        for col_desc in column_desc:
            if "id" in col_desc:
                self.es.index(
                    index=self.idx_desc,
                    id=col_desc['id'],
                    doc_type=self.doc_type_description,
                    body=col_desc)

    # upload sensor data
    def upload_sensoren_tsv(self, sensor_data):
        for idx, row in sensor_data.iterrows():
            obj_body = row.to_json()
            self.es.index(
                index=self.idx_desc,
                id='sensoren ' + str(idx),
                doc_type=self.doc_type_description,
                body=obj_body)

    # upload tsv
    def upload_data(self, tsv_name, tsv_data, json_data):
        def generate_actions(data):
            for idx, row in data.iterrows():
                yield {
                    '_op_type': 'index',
                    '_index': self.idx_data,
                    '_id': idx,
                    '_type': self.doc_type_metadata,
                    '_source': row.to_json()
                }

        # Performance boost by setting stopping index refreshing
        self.es.indices.put_settings(
            index=self.idx_data, body={'index': {
                "refresh_interval": '-1'
            }})
        tsv_data['filename'] = tsv_name
        tsv_data = tsv_data.set_index("unique_id")
        for success, info in helpers.parallel_bulk(
                self.es,
                generate_actions(tsv_data),
                thread_count=4,
                chunk_size=500):
            pass
        columns_df = pd.DataFrame(json_data['columns'])
        columns_df.columns = ["ids"]
        columns_df = columns_df[columns_df["ids"] != "unique_id"]
        obj_body = dict()
        obj_body['ids'] = columns_df["ids"].values.tolist()
        obj_body['file_name'] = tsv_name
        obj_body['objects'] = len(tsv_data)
        obj_body['update_date'] = strftime("%Y-%m-%d %H:%M:%S", localtime())
        self.es.index(
            index=self.idx_file_names,
            doc_type=self.doc_type_file_names,
            id=tsv_name,
            body=json.dumps(obj_body))
        self.es.indices.refresh(index=self.idx_data)
        self.es.indices.put_settings(
            index=self.idx_data, body={'index': {
                "refresh_interval": '3s'
            }})

    def query(self, idx, query):
        res = self.es.search(index=idx, body=query)
        return res

    def mquery(self, requests):
        res = self.es.msearch(body=requests)
        return res
